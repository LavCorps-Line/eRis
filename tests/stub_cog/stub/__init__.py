#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# /tests/stub_cog/stub
## a component of eRis
* example cog for testing loader
"""

# First-Party Imports #
from eris.cog import CogImportData

# Local Imports #
from .stub import stub


def data(**kwargs) -> CogImportData:
    """Cog Import Data Shim"""
    return CogImportData(cog=stub(**kwargs))
