#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# /tests/stub_cog/stub
## a component of eRis
* example cog for testing loader
"""

# First-Party Imports #
import eris.bot
import eris.cog


class stub(eris.cog.Cog):
    """stub cog for testing"""

    def __init__(self, **kwargs) -> None:
        self.bot: eris.bot.Bot = kwargs["bot"]
