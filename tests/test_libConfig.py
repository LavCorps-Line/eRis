#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# tests/test_libConfig.py
## a component of eRis
* handles testing of eris/cogs/libCore
"""

# Standard Library Imports #
import asyncio
import random

# Third-Party Imports #
import pytest

# First-Party Imports #
import eris.bot
import eris.exceptions
from eris.cogs.libConfig import libConfig as libCfg
from eris.cogs.libConfig.drivers.abc import cache as _cache
from eris.cogs.libConfig.lockman import lockManager as lockman


class testSnowflake:

    def __init__(self):
        self.id = random.randint(0, 2**31 - 1)


class testCogData:

    def __init__(self):
        self.name = "cog_test"


class testCog:

    def __init__(self):
        self.__data__ = testCogData()


@pytest.mark.asyncio
async def test_libConfig_sqlite_io(bot: eris.bot.Bot):
    while "libConfig" not in bot.cogs:
        await asyncio.sleep(0)
    libConfig: libCfg = bot.cogs["libConfig"]
    snowflake = random.randint(0, 2**31 - 1)
    special = random.randint(0, 2**31 - 1)
    await libConfig.write(snf=snowflake, cog="cog_test", data={"special": special})
    assert await libConfig.read(snf=snowflake, cog="cog_test") == {"special": special}


@pytest.mark.asyncio
async def test_libConfig_sqlite_cachebusting_io(bot: eris.bot.Bot):
    while "libConfig" not in bot.cogs:
        await asyncio.sleep(0)
    libConfig: libCfg = bot.cogs["libConfig"]
    snowflake = random.randint(0, 2**31 - 1)
    special = random.randint(0, 2**31 - 1)
    await libConfig.write(snf=snowflake, cog="cog_test", data={"special": special})
    libConfig.driver.cache.data.clear()
    assert await libConfig.read(snf=snowflake, cog="cog_test") == {"special": special}


@pytest.mark.asyncio
async def test_libConfig_sqlite_lockman_io(bot: eris.bot.Bot):
    while "libConfig" not in bot.cogs:
        await asyncio.sleep(0)
    special = random.randint(0, 2**31 - 1)
    cog = testCog()
    snf = testSnowflake()
    async with lockman(snf=snf, bot=bot, cog=cog) as data:
        data["special"] = special
    async with lockman(snf=snf, bot=bot, cog=cog) as data:
        assert data == {"special": special}


@pytest.mark.asyncio
async def test_libConfig_psql_io(bot_psql: eris.bot.Bot):
    while "libConfig" not in bot_psql.cogs:
        await asyncio.sleep(0)
    libConfig: libCfg = bot_psql.cogs["libConfig"]
    snowflake = random.randint(0, 2**31 - 1)
    special = random.randint(0, 2**31 - 1)
    await libConfig.write(snf=snowflake, cog="cog_test", data={"special": special})
    assert await libConfig.read(snf=snowflake, cog="cog_test") == {"special": special}


@pytest.mark.asyncio
async def test_libConfig_psql_cachebusting_io(bot_psql: eris.bot.Bot):
    while "libConfig" not in bot_psql.cogs:
        await asyncio.sleep(0)
    libConfig: libCfg = bot_psql.cogs["libConfig"]
    snowflake = random.randint(0, 2**31 - 1)
    special = random.randint(0, 2**31 - 1)
    await libConfig.write(snf=snowflake, cog="cog_test", data={"special": special})
    libConfig.driver.cache.data.clear()
    assert await libConfig.read(snf=snowflake, cog="cog_test") == {"special": special}


@pytest.mark.asyncio
async def test_libConfig_psql_lockman_io(bot_psql: eris.bot.Bot):
    while "libConfig" not in bot_psql.cogs:
        await asyncio.sleep(0)
    special = random.randint(0, 2**31 - 1)
    cog = testCog()
    snf = testSnowflake()
    async with lockman(snf=snf, bot=bot_psql, cog=cog) as data:
        data["special"] = special
    async with lockman(snf=snf, bot=bot_psql, cog=cog) as data:
        assert data == {"special": special}


@pytest.mark.asyncio
async def test_libConfig_sqlite_io_error_handling(bot: eris.bot.Bot):
    while "libConfig" not in bot.cogs:
        await asyncio.sleep(0)
    libConfig: libCfg = bot.cogs["libConfig"]
    try:
        await libConfig.read(snf=random.randint(0, 2**31 - 1), cog="cog_test")
    except eris.exceptions.DataNotFound as daNoFo:
        assert daNoFo


@pytest.mark.asyncio
async def test_libConfig_psql_io_error_handling(bot_psql: eris.bot.Bot):
    while "libConfig" not in bot_psql.cogs:
        await asyncio.sleep(0)
    libConfig: libCfg = bot_psql.cogs["libConfig"]
    try:
        await libConfig.read(snf=random.randint(0, 2**31 - 1), cog="cog_test")
    except eris.exceptions.DataNotFound as daNoFo:
        assert daNoFo


@pytest.mark.asyncio
async def test_libConfig_sqlite_removed_guild_cleanup(bot: eris.bot.Bot):
    while "libConfig" not in bot.cogs:
        await asyncio.sleep(0)
    libConfig: libCfg = bot.cogs["libConfig"]
    guild = testSnowflake()
    await libConfig.write(snf=guild.id, cog="cog_test", data={1: 3, 2: 2, 3: 1})
    await libConfig._removed_guild_cleanup(guild)
    await asyncio.sleep(3.5)
    try:
        assert not await libConfig.read(snf=guild.id, cog="cog_test")
    except Exception as err:
        assert isinstance(err, eris.exceptions.DataNotFound)


@pytest.mark.asyncio
async def test_libConfig_psql_removed_guild_cleanup(bot_psql: eris.bot.Bot):
    while "libConfig" not in bot_psql.cogs:
        await asyncio.sleep(0)
    libConfig: libCfg = bot_psql.cogs["libConfig"]
    guild = testSnowflake()
    await libConfig.write(snf=guild.id, cog="cog_test", data={1: 3, 2: 2, 3: 1})
    await libConfig._removed_guild_cleanup(guild)
    await asyncio.sleep(3.5)
    try:
        assert not await libConfig.read(snf=guild.id, cog="cog_test")
    except Exception as err:
        assert isinstance(err, eris.exceptions.DataNotFound)


@pytest.mark.asyncio
async def test_libConfig_sqlite_removed_channel_cleanup(bot: eris.bot.Bot):
    while "libConfig" not in bot.cogs:
        await asyncio.sleep(0)
    libConfig: libCfg = bot.cogs["libConfig"]
    channel = testSnowflake()
    await libConfig.write(snf=channel.id, cog="cog_test", data={1: 3, 2: 2, 3: 1})
    await libConfig._removed_channel_cleanup(channel)
    await asyncio.sleep(3.5)
    try:
        assert not await libConfig.read(snf=channel.id, cog="cog_test")
    except Exception as err:
        assert isinstance(err, eris.exceptions.DataNotFound)


@pytest.mark.asyncio
async def test_libConfig_psql_removed_channel_cleanup(bot_psql: eris.bot.Bot):
    while "libConfig" not in bot_psql.cogs:
        await asyncio.sleep(0)
    libConfig: libCfg = bot_psql.cogs["libConfig"]
    channel = testSnowflake()
    await libConfig.write(snf=channel.id, cog="cog_test", data={1: 3, 2: 2, 3: 1})
    await libConfig._removed_channel_cleanup(channel)
    await asyncio.sleep(3.5)
    try:
        assert not await libConfig.read(snf=channel.id, cog="cog_test")
    except Exception as err:
        assert isinstance(err, eris.exceptions.DataNotFound)


@pytest.mark.asyncio
async def test_libConfig_sqlite_removed_message_cleanup(bot: eris.bot.Bot):
    while "libConfig" not in bot.cogs:
        await asyncio.sleep(0)
    libConfig: libCfg = bot.cogs["libConfig"]
    message = testSnowflake()
    await libConfig.write(snf=message.id, cog="cog_test", data={1: 3, 2: 2, 3: 1})
    await libConfig._removed_message_cleanup(message)
    await asyncio.sleep(3.5)
    try:
        assert not await libConfig.read(snf=message.id, cog="cog_test")
    except Exception as err:
        assert isinstance(err, eris.exceptions.DataNotFound)


@pytest.mark.asyncio
async def test_libConfig_psql_removed_message_cleanup(bot_psql: eris.bot.Bot):
    while "libConfig" not in bot_psql.cogs:
        await asyncio.sleep(0)
    libConfig: libCfg = bot_psql.cogs["libConfig"]
    message = testSnowflake()
    await libConfig.write(snf=message.id, cog="cog_test", data={1: 3, 2: 2, 3: 1})
    await libConfig._removed_message_cleanup(message)
    await asyncio.sleep(3.5)
    try:
        assert not await libConfig.read(snf=message.id, cog="cog_test")
    except Exception as err:
        assert isinstance(err, eris.exceptions.DataNotFound)


@pytest.mark.asyncio
async def test_libConfig_cache_io(cache: _cache):
    snf = random.randint(0, 2**31 - 1)
    cname = "cog_test"
    data = {1: 3, 2: 2, 3: 1}
    cache.push(snf, cname, data)
    assert cache.pull(snf, cname) == data


@pytest.mark.asyncio
async def test_libConfig_cache_checking(cache: _cache):
    snf = random.randint(0, 2**31 - 1)
    cname = "cog_test"
    data = {1: 3, 2: 2, 3: 1}
    cache.push(snf, cname, data)
    assert cache.check(snf, cname)


@pytest.mark.asyncio
async def test_libConfig_cache_pullfail(cache: _cache):
    try:
        cache.pull(random.randint(0, 2**31 - 1), "cog_test")
    except IndexError:
        assert True
    else:
        assert False


@pytest.mark.asyncio
async def test_libConfig_cache_check_outdated(cache: _cache):
    snf = random.randint(0, 2**31 - 1)
    cname = "cog_test"
    data = {1: 3, 2: 2, 3: 1}
    cache.push(snf, cname, data)
    cache.data[snf][cname].age = -(2**15 - 1)
    assert not cache.check(snf, cname)


@pytest.mark.asyncio
async def test_libConfig_cache_cleanup_outdated(cache: _cache):
    snf = random.randint(0, 2**31 - 1)
    cname = "cog_test"
    data = {1: 3, 2: 2, 3: 1}
    cache.push(snf, cname, data)
    cache.data[snf][cname].age = -(2**15 - 1)
    assert cache.cleanup() == 1
