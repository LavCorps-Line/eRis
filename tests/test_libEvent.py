#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# tests/test_libEvent.py
## a component of eRis
* handles testing of eris/cogs/libCore
"""

# Standard Library Imports #
import asyncio
import random

# Third-Party Imports #
import pytest

# First-Party Imports #
import eris.bot
from eris.cogs.libEvent import libEvent as _libEvent


@pytest.mark.asyncio
async def test_libEvent_on_connect(bot: eris.bot.Bot):
    while "libEvent" not in bot.coreCogs:
        await asyncio.sleep(0)
    libEvent: _libEvent = bot.coreCogs["libEvent"]
    await libEvent.connectedHandler()


@pytest.mark.asyncio
async def test_libEvent_on_disconnect(bot: eris.bot.Bot):
    while "libEvent" not in bot.coreCogs:
        await asyncio.sleep(0)
    libEvent: _libEvent = bot.coreCogs["libEvent"]
    await libEvent.disconnectedHandler()


@pytest.mark.asyncio
async def test_libEvent_on_resumed(bot: eris.bot.Bot):
    while "libEvent" not in bot.coreCogs:
        await asyncio.sleep(0)
    libEvent: _libEvent = bot.coreCogs["libEvent"]
    await libEvent.resumedHandler()


@pytest.mark.asyncio
async def test_libEvent_on_shard_connect(bot: eris.bot.Bot):
    while "libEvent" not in bot.coreCogs:
        await asyncio.sleep(0)
    libEvent: _libEvent = bot.coreCogs["libEvent"]
    await libEvent.shardConnectedHandler(random.randint(0, 2**32 - 1))


@pytest.mark.asyncio
async def test_libEvent_on_shard_disconnect(bot: eris.bot.Bot):
    while "libEvent" not in bot.coreCogs:
        await asyncio.sleep(0)
    libEvent: _libEvent = bot.coreCogs["libEvent"]
    await libEvent.shardDisconnectedHandler(random.randint(0, 2**32 - 1))


@pytest.mark.asyncio
async def test_libEvent_on_shard_resumed(bot: eris.bot.Bot):
    while "libEvent" not in bot.coreCogs:
        await asyncio.sleep(0)
    libEvent: _libEvent = bot.coreCogs["libEvent"]
    await libEvent.shardResumedHandler(random.randint(0, 2**32 - 1))
