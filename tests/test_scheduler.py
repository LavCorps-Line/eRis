#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# tests/test_scheduler.py
## a component of eRis
* handles testing of eris/scheduler.py
"""

# Standard Library Imports #
import asyncio

# Third-Party Imports #
import pytest

# First-Party Imports #
import eris.bot


@pytest.mark.asyncio
async def test_scheduler_init(bot: eris.bot.Bot):
    scheduler = bot.scheduler
    assert scheduler.operating


@pytest.mark.asyncio
async def test_scheduler_schedule(bot: eris.bot.Bot):
    async def a(c: list):
        c.pop()

    b = []
    assert len(b) == 0
    b.append(None)
    assert len(b) == 1
    coro = a(b)
    scheduler = bot.scheduler
    scheduler.schedule_task(coro, offset=1)
    await asyncio.sleep(2)
    assert scheduler.operating
    assert len(b) == 0


@pytest.mark.asyncio
async def test_scheduler_schedule_runninglist(bot: eris.bot.Bot):
    async def a():
        pass

    coro = a()
    scheduler = bot.scheduler
    scheduler.schedule_task(coro, offset=1)
    assert scheduler.operating
    assert scheduler.schedule.qsize() == 1
    await asyncio.sleep(2)
    assert scheduler.operating
    assert scheduler.schedule.qsize() == 0


@pytest.mark.asyncio
async def test_scheduler_schedule_flush(bot: eris.bot.Bot):
    async def a():
        pass

    coro = a()
    scheduler = bot.scheduler
    scheduler.schedule_task(coro, offset=30)
    assert scheduler.operating
    assert scheduler.schedule.qsize() == 1
    await scheduler.stop()
    assert not scheduler.operating
    assert scheduler.schedule.qsize() == 1
    await scheduler.flush()
    assert not scheduler.operating
    assert scheduler.schedule.qsize() == 0


@pytest.mark.asyncio
async def test_scheduler_schedule_callback(bot: eris.bot.Bot):
    async def a():
        pass

    coro = a()
    b = set()
    scheduler = bot.scheduler
    scheduler.schedule_task(coro, callbacks=[b.add])
    await asyncio.sleep(1)
    assert scheduler.operating
    assert len(b) == 1


@pytest.mark.asyncio
async def test_scheduler_sanity_start(bot: eris.bot.Bot):
    scheduler = bot.scheduler
    try:
        await scheduler.start()
    except RuntimeError:
        assert True
    else:
        assert False


@pytest.mark.asyncio
async def test_scheduler_sanity_flushstart(bot: eris.bot.Bot):
    scheduler = bot.scheduler
    await scheduler.stop()
    scheduler._flushing = True
    try:
        await scheduler.start()
    except RuntimeError:
        assert True
    else:
        assert False


@pytest.mark.asyncio
async def test_scheduler_sanity_stop(bot: eris.bot.Bot):
    scheduler = bot.scheduler
    await scheduler.stop()
    try:
        await scheduler.stop()
    except RuntimeError:
        await scheduler.start()
        assert True
    else:
        assert False


@pytest.mark.asyncio
async def test_scheduler_sanity_flush(bot: eris.bot.Bot):
    scheduler = bot.scheduler
    await scheduler.flush()
    assert not scheduler._flushing
    await scheduler.stop()
    scheduler._flushing = True
    await scheduler.flush()
    assert scheduler._flushing
    scheduler._flushing = False
    await scheduler.start()
