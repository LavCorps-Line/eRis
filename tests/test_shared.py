#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# tests/test_shared.py
## a component of eRis
* handles testing of eris/shared.py
"""

# First-Party Imports #
import eris.shared


def test_shared_exitCodes():
    assert (
        eris.shared.exit_codes.error == 1
        and eris.shared.exit_codes.shutdown == 0
        and eris.shared.exit_codes.restart == 39
    )


def test_shared_shutdownCauses():
    assert (
        eris.shared.shutdown_causes.user_requested_stop == 0
        and eris.shared.shutdown_causes.user_requested_restart == 1
        and eris.shared.shutdown_causes.unhandled_exception == 2
        and eris.shared.shutdown_causes.caught_signal == 3
    )
