#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# tests/test_libCore.py
## a component of eRis
* handles testing of eris/cogs/libCore
"""

# Standard Library Imports #
import asyncio
import datetime
import os

# Third-Party Imports #
import discord
import pytest

# First-Party Imports #
import eris.bot
import eris.cog
from eris.cogs.libCore import libCore as _libCore
from eris.cogs.libCore.eRisHelpCommand import eRisHelpCommand as _eHelpCommand

endingNote = "Type help command for more info on a command. You can also type help category for more info on a category."


def test_libCore_shortenText(bot: eris.bot.Bot):
    libCore: _libCore = bot.cogs["libCore"]
    assert libCore.shortenText("1 3 5", width=6) == "1 3..."
    assert libCore.shortenText("1 / 2", breakspace=["/"]) == "1"
    assert libCore.shortenText("1_2", whitespace=["_"]) == "1 2"
    assert libCore.shortenText("1 2", joinspace="_") == "1_2"


def test_libCore_splitText(bot: eris.bot.Bot):
    libCore: _libCore = bot.cogs["libCore"]
    assert libCore.splitText("A (B)") == ["A", "B"]


def test_libCore_listDiff(bot: eris.bot.Bot):
    libCore: _libCore = bot.cogs["libCore"]
    diffs = libCore.listDiff(["A", "B"], ["B", "C"])

    assert diffs.remove == ["A"]
    assert diffs.removed == ["A"]
    assert diffs.before == ["A"]

    assert diffs.now == ["B"]
    assert diffs.unchanged == ["B"]

    assert diffs.add == ["C"]
    assert diffs.added == ["C"]
    assert diffs.after == ["C"]


def test_libCore_pagifyString(bot: eris.bot.Bot):
    libCore: _libCore = bot.cogs["libCore"]
    assert [_ for _ in libCore.pagifyString("1 2 3 4 5", pageLength=1)] == [
        "1",
        "2",
        "3",
        "4",
        "5",
    ]
    assert [_ for _ in libCore.pagifyString("1_2_3_4_5", whitespace=["_"], pageLength=1)] == [
        "1",
        "2",
        "3",
        "4",
        "5",
    ]


def test_chronologize_localTz(bot: eris.bot.Bot):
    libCore: _libCore = bot.cogs["libCore"]
    assert (
        libCore.chronologize.localTz
        == datetime.datetime.now(datetime.timezone.utc).astimezone().tzinfo
    )


def test_chronologize_now(bot: eris.bot.Bot):
    libCore: _libCore = bot.cogs["libCore"]
    assert isinstance(libCore.chronologize.now(), datetime.datetime)


def test_chronologize_isoNow(bot: eris.bot.Bot):
    libCore: _libCore = bot.cogs["libCore"]
    assert datetime.datetime.fromisoformat(libCore.chronologize.isoNow())


def test_chronologize_posixNow(bot: eris.bot.Bot):
    libCore: _libCore = bot.cogs["libCore"]
    assert isinstance(libCore.chronologize.posixNow(), float)


def test_naturalize_list_listContents(bot: eris.bot.Bot):
    libCore: _libCore = bot.cogs["libCore"]
    assert libCore.naturalize.list.listContents(["A", "B", "C"]) == "A, B, and C"


def test_naturalize_list_listContents_withEmptyList(bot: eris.bot.Bot):
    libCore: _libCore = bot.cogs["libCore"]
    assert libCore.naturalize.list.listContents([]) == ""


def test_helpCommand_endingNote():
    eHelp = _eHelpCommand()
    assert eHelp.endingNote == endingNote


def test_helpCommand_embedFactory():
    eHelp = _eHelpCommand()
    embed = eHelp.embedFactory(title="Test Title", description="Test Description")
    assert embed.title == "Test Title"
    assert embed.description == "Test Description"
    assert isinstance(embed.timestamp, datetime.datetime)
    assert embed.author.name == "eRis"
    assert embed.footer.text == endingNote


@pytest.mark.asyncio
async def test_ping(bot: eris.bot.Bot):
    while "libCore" not in bot.coreCogs:
        await asyncio.sleep(0)
    testChannel: discord.TextChannel = bot.get_channel(
        int(os.environ["CI_TEST_CHANNEL_SNOWFLAKE"])
    )
    testMessage: discord.Message = await testChannel.fetch_message(
        int(os.environ["CI_TEST_MESSAGE_SNOWFLAKE"])
    )
    ctx = await bot.get_context(testMessage)
    await ctx.invoke(bot.get_command("ping"))


@pytest.mark.asyncio
async def test_invite(bot: eris.bot.Bot):
    while "libCore" not in bot.coreCogs:
        await asyncio.sleep(0)
    testChannel: discord.TextChannel = bot.get_channel(
        int(os.environ["CI_TEST_CHANNEL_SNOWFLAKE"])
    )
    testMessage: discord.Message = await testChannel.fetch_message(
        int(os.environ["CI_TEST_MESSAGE_SNOWFLAKE"])
    )
    ctx = await bot.get_context(testMessage)
    await ctx.invoke(bot.get_command("invite"))


@pytest.mark.asyncio
async def test_helpCommand_bot(bot: eris.bot.Bot):
    while not bot.help_command:
        await asyncio.sleep(0)
    testChannel: discord.TextChannel = bot.get_channel(
        int(os.environ["CI_TEST_CHANNEL_SNOWFLAKE"])
    )
    testMessage: discord.Message = await testChannel.fetch_message(
        int(os.environ["CI_TEST_MESSAGE_SNOWFLAKE"])
    )
    ctx = await bot.get_context(testMessage)
    await ctx.send_help()


@pytest.mark.asyncio
async def test_helpCommand_cog(bot: eris.bot.Bot):
    while not bot.help_command:
        await asyncio.sleep(0)
    testChannel: discord.TextChannel = bot.get_channel(
        int(os.environ["CI_TEST_CHANNEL_SNOWFLAKE"])
    )
    testMessage: discord.Message = await testChannel.fetch_message(
        int(os.environ["CI_TEST_MESSAGE_SNOWFLAKE"])
    )
    ctx = await bot.get_context(testMessage)
    await ctx.send_help(bot.coreCogs["libCore"])


@pytest.mark.asyncio
async def test_helpCommand_command(bot: eris.bot.Bot):
    while not bot.help_command:
        await asyncio.sleep(0)
    testChannel: discord.TextChannel = bot.get_channel(
        int(os.environ["CI_TEST_CHANNEL_SNOWFLAKE"])
    )
    testMessage: discord.Message = await testChannel.fetch_message(
        int(os.environ["CI_TEST_MESSAGE_SNOWFLAKE"])
    )
    ctx = await bot.get_context(testMessage)
    await ctx.send_help(bot.get_command("help"))
