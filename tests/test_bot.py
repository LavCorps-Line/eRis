#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# tests/test_bot.py
## a component of eRis
* handles testing of eris/bot.py
"""

# Standard Library Imports #
import asyncio

# Third-Party Imports #
import discord.ext.commands as commands
import pytest

# First-Party Imports #
from eris.bot import Bot
from eris.handler import handler as _handler


@pytest.mark.asyncio
async def test_bot_init():
    assert Bot(
        command_prefix=commands.when_mentioned_or(""),
        loop=asyncio.get_running_loop(),
        handler=_handler(),
    )
