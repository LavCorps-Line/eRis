#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# tests/conftest.py
## a component of eRis
* provides fixtures used by all tests
"""

# Standard Library Imports #
import asyncio
import os
import random
import signal
import string

# Third-Party Imports #
import pytest
import pytest_asyncio

# First-Party Imports #
import eris
import eris.bot
import eris.logger
from eris.cogs.libConfig.drivers.abc import cache as _cache
from eris.handler import handler as _handler
from eris.logger import logger
from eris.supervisor.__main__ import eRisEnvironment

# Defining cache fixture


@pytest.fixture
def cache() -> _cache:
    """Yields a cache"""
    _c = _cache()
    yield _c
    _c.data.clear()


# Defining bot fixture


@pytest_asyncio.fixture
async def bot() -> eris.bot.Bot:
    """Yields a fully functional bot"""
    fixtureID = "".join(random.choices(string.hexdigits.lower(), k=16))
    environment = eRisEnvironment(
        ERIS_DISCORD_TOKEN=os.environ["DISCORD_BOT_TEST_TOKEN"],
        ERIS_CONFIG_TYPE="SQLITE",
        ERIS_SQLITE_DBPATH=f"/tmp/{fixtureID}.db",
        ERIS_LOGGING_DIR=f"{os.environ['CI_PROJECT_DIR']}/logs/{fixtureID}",
        ERIS_LOGGING_LEVEL="DEBUG",
    )
    for var in environment.env:
        os.environ[var] = environment.env[var]
    eris.__main__.environmentVerify()

    log = logger("eRis.main")  # Initialize and keep a reference to our eRis.main logger
    discordLog = logger("discord")  # Initialize and keep a reference to discord.py's logger
    discordLog.setLevel("WARNING")
    handler = _handler()  # Initialize handler
    loop = asyncio.get_running_loop()
    bot = eris.__main__.botInit(loop=loop, handler=handler)

    eris.__main__.signalRegister(loop=loop, handler=handler, bot=bot, log=log)
    await eris.__main__.eRisAuth(bot, log)
    bot.connTask = loop.create_task(bot.connect())
    await eris.__main__.eRisInit(bot, log)

    log.info(f"Fixture {fixtureID} initialized")
    yield bot
    if handler.shutdownTask:
        await handler.shutdownTask
    else:
        await handler.shutdownHandler(
            bot, cause=handler.shutdownCauses.caught_signal, signal=signal.SIGINT
        )


@pytest_asyncio.fixture
async def bot_psql() -> eris.bot.Bot:
    """Yields a fully functional bot"""
    fixtureID = "".join(random.choices(string.hexdigits.lower(), k=16))
    environment = eRisEnvironment(
        ERIS_DISCORD_TOKEN=os.environ["DISCORD_BOT_TEST_TOKEN"],
        ERIS_CONFIG_TYPE="PSQL",
        ERIS_PSQL_DBNAME=os.environ["POSTGRES_DB"],
        ERIS_PSQL_USERNAME=os.environ["POSTGRES_USER"],
        ERIS_PSQL_PASSWORD=os.environ["POSTGRES_PASSWORD"],
        ERIS_PSQL_TABLE=fixtureID,
        ERIS_PSQL_HOST="eris-postgres-testing",
        ERIS_LOGGING_DIR=f"{os.environ['CI_PROJECT_DIR']}/logs/{fixtureID}",
        ERIS_LOGGING_LEVEL="DEBUG",
    )
    for var in environment.env:
        os.environ[var] = environment.env[var]
    eris.__main__.environmentVerify()

    log = logger("eRis.main")  # Initialize and keep a reference to our eRis.main logger
    discordLog = logger("discord")  # Initialize and keep a reference to discord.py's logger
    discordLog.setLevel("WARNING")
    handler = _handler()  # Initialize handler
    loop = asyncio.get_running_loop()
    bot_psql = eris.__main__.botInit(loop=loop, handler=handler)

    eris.__main__.signalRegister(loop=loop, handler=handler, bot=bot_psql, log=log)
    await eris.__main__.eRisAuth(bot_psql, log)
    bot_psql.connTask = loop.create_task(bot_psql.connect())
    await eris.__main__.eRisInit(bot_psql, log)

    log.info(f"PSQL Fixture {fixtureID} initialized")
    yield bot_psql
    if handler.shutdownTask:
        await handler.shutdownTask
    else:
        await handler.shutdownHandler(
            bot_psql, cause=handler.shutdownCauses.caught_signal, signal=signal.SIGINT
        )


# Defining context fixture


class _testContext:

    def __init__(self, bot):
        self.args = []
        self.author = None
        self.bot = bot
        self.bot_permissions = None
        self.channel = None
        self.clean_prefix = ">"
        self.cog = None
        self.command = None
        self.command_failed = False
        self.current_argument = None
        self.current_parameter = None
        self.guild = None
        self.interaction = None
        self.invoked_parents = None
        self.invoked_subcommand = None
        self.invoked_with = "test"
        self.kwargs = {}
        self.me = None
        self.message = "Test Message"
        self.permissions = None
        self.prefix = ">"
        self.subcommand_passed = None
        self.valid = None
        self.voice_client = None
