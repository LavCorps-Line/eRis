#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# tests/test_main.py
## a component of eRis
* handles testing of eris/__main__.py
"""

# Standard Library Imports #
import asyncio
import os
import signal

# Third-Party Imports #
import pytest
import toml

# First-Party Imports #
import eris.supervisor


@pytest.mark.asyncio
async def test_supervisor_main():
    try:
        with open("/etc/eris", mode="w") as file:
            toml.dump(
                {
                    "ERIS_DISCORD_TOKEN": os.environ["DISCORD_BOT_TEST_TOKEN"],
                    "ERIS_CONFIG_TYPE": "SQLITE",
                    "ERIS_SQLITE_DBPATH": "/tmp/test_supervisor.db",
                },
                file,
            )
    except PermissionError:
        os.environ["ERIS_DISCORD_TOKEN"] = os.environ["DISCORD_BOT_TEST_TOKEN"]
        os.environ["ERIS_CONFIG_TYPE"] = "SQLITE"
        os.environ["ERIS_SQLITE_DBPATH"] = "/tmp/test_supervisor.db"
        eris.supervisor.__main__.environmentVerify()
        environment = eris.supervisor.__main__.loadEnvironment()
        eris.supervisor.__main__.setPerms(environment=environment)
    else:
        eris.supervisor.__main__.environmentVerify()
        environment = eris.supervisor.__main__.loadEnvironment()
        eris.supervisor.__main__.setPerms(environment=environment)

    supervisor = eris.supervisor.__main__.Supervisor(loop=asyncio.get_running_loop())

    await supervisor.spawn_eris()
    first_supervisor = asyncio.get_running_loop().create_task(supervisor.supervise_eris())
    first_supervisor.add_done_callback(supervisor.supervisor_callback)

    await asyncio.sleep(10)
    supervisor.forward_signal(signal.SIGINT)
    await asyncio.sleep(10)
    assert supervisor.eRis is None
    assert supervisor.supervisor is None
