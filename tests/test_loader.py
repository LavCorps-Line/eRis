#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# tests/test_loader.py
## a component of eRis
* handles testing of eris/cogs/loader
"""

# Standard Library Imports #
import asyncio
import os
import pathlib

# Third-Party Imports #
import discord
import pytest

# First-Party Imports #
import eris.bot
import eris.exceptions as exceptions
from eris.cogs.loader import loader as _loader_cog


@pytest.mark.asyncio
async def test_loader_cog_lifecycle(bot: eris.bot.Bot):
    while "loader" not in bot.coreCogs:  # Bad idea :/
        await asyncio.sleep(0)
    loader: _loader_cog = bot.coreCogs["loader"]
    await loader._loader__load_cog(
        cogName="stub",
        cogPath=f"{os.environ['CI_PROJECT_DIR']}/tests/stub_cog/data.toml",
        _retry=False,
    )
    await loader._loader__unload_cog(cogName="stub")


@pytest.mark.asyncio
async def test_loader_cog_lifecycle_pathlib(bot: eris.bot.Bot):
    while "loader" not in bot.coreCogs:  # Bad idea :/
        await asyncio.sleep(0)
    loader: _loader_cog = bot.coreCogs["loader"]
    await loader._loader__load_cog(
        cogName="stub",
        cogPath=pathlib.Path(f"{os.environ['CI_PROJECT_DIR']}/tests/stub_cog/data.toml"),
        _retry=False,
    )
    await loader._loader__unload_cog(cogName="stub")


@pytest.mark.asyncio
async def test_loader_exceptions_missing_dependencies(bot: eris.bot.Bot):
    while "loader" not in bot.coreCogs:  # Bad idea :/
        await asyncio.sleep(0)
    loader: _loader_cog = bot.coreCogs["loader"]
    errList = []
    try:
        await loader._loader__load_cog(
            cogName="stub_nodeps",
            cogPath=f"{os.environ['CI_PROJECT_DIR']}/tests/stub_cog/data.toml",
            _retry=False,
        )
    except exceptions.DependencyNotLoaded as DeNoLo:
        errList.append(DeNoLo)
    assert len(errList) == 1


@pytest.mark.asyncio
async def test_loader_exceptions_malformed(bot: eris.bot.Bot):
    while "loader" not in bot.coreCogs:  # Bad idea :/
        await asyncio.sleep(0)
    loader: _loader_cog = bot.coreCogs["loader"]
    errList = []
    try:
        await loader._loader__load_cog(
            cogName="stub_malformed",
            cogPath=f"{os.environ['CI_PROJECT_DIR']}/tests/stub_cog/data.toml",
            _retry=False,
        )
    except FileNotFoundError as FiNoFoEr:
        errList.append(FiNoFoEr)
    assert len(errList) == 1


@pytest.mark.asyncio
async def test_loader_exceptions_load_not_found(bot: eris.bot.Bot):
    while "loader" not in bot.coreCogs:  # Bad idea :/
        await asyncio.sleep(0)
    loader: _loader_cog = bot.coreCogs["loader"]
    errList = []
    try:
        await loader._loader__load_cog(
            cogName="stub_not_found",
            cogPath=f"{os.environ['CI_PROJECT_DIR']}/tests/stub_cog/data.toml",
            _retry=False,
        )
    except exceptions.CogNotFound as CoNoFo:
        errList.append(CoNoFo)
    assert len(errList) == 1


@pytest.mark.asyncio
async def test_loader_exceptions_unload_core(bot: eris.bot.Bot):
    while "loader" not in bot.coreCogs:  # Bad idea :/
        await asyncio.sleep(0)
    loader: _loader_cog = bot.coreCogs["loader"]
    errList = []
    try:
        await loader._loader__unload_cog("libConfig")
    except exceptions.CoreUnloadAttempt as CoUnAt:
        errList.append(CoUnAt)
    assert len(errList) == 1


@pytest.mark.asyncio
async def test_loader_exceptions_unload_nonexistent(bot: eris.bot.Bot):
    while "loader" not in bot.coreCogs:  # Bad idea :/
        await asyncio.sleep(0)
    loader: _loader_cog = bot.coreCogs["loader"]
    errList = []
    try:
        await loader._loader__unload_cog("foo")
    except exceptions.CogNotFound as CoNoFo:
        errList.append(CoNoFo)
    assert len(errList) == 1


@pytest.mark.asyncio
async def test_invite(bot: eris.bot.Bot):
    while "loader" not in bot.coreCogs:
        await asyncio.sleep(0)
    testChannel: discord.TextChannel = bot.get_channel(
        int(os.environ["CI_TEST_CHANNEL_SNOWFLAKE"])
    )
    testMessage: discord.Message = await testChannel.fetch_message(
        int(os.environ["CI_TEST_MESSAGE_SNOWFLAKE"])
    )
    ctx = await bot.get_context(testMessage)
    await ctx.invoke(bot.get_command("cogs"))


@pytest.mark.asyncio
async def test_load_unload(bot: eris.bot.Bot):
    while "loader" not in bot.coreCogs:
        await asyncio.sleep(0)
    testChannel: discord.TextChannel = bot.get_channel(
        int(os.environ["CI_TEST_CHANNEL_SNOWFLAKE"])
    )
    testMessage: discord.Message = await testChannel.fetch_message(
        int(os.environ["CI_TEST_MESSAGE_SNOWFLAKE"])
    )
    ctx = await bot.get_context(testMessage)
    await ctx.invoke(
        bot.get_command("load"),
        cog_name="stub",
        cog_path=f"{os.environ['CI_PROJECT_DIR']}/tests/stub_cog/data.toml",
    )
    await asyncio.sleep(1)
    await ctx.invoke(bot.get_command("unload"), cog_name="stub")
