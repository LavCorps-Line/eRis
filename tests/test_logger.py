#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# tests/test_logger.py
## a component of eRis
* handles testing of eris/shared.py
"""

# Standard Library Imports #
import datetime
import logging

# First-Party Imports #
import eris.logger


def test_logger_formatTime():
    log = eris.logger.logger("eRis.pytest")
    assert datetime.datetime.fromisoformat(
        log.formatTime(
            record=logging.LogRecord(
                level=30,
                pathname="",
                lineno=1,
                msg="",
                args=None,
                exc_info=(None, None, None),
                name="",
            )
        )
    )


def test_logger_formatStack():
    log = eris.logger.logger("eRis.pytest")
    assert log.formatStack("Test String") == "Test String"


def test_logger_logLevels():
    assert (
        eris.logger.logLevels.NOTSET == 0
        and eris.logger.logLevels.DEBUG == 10
        and eris.logger.logLevels.INFO == 20
        and eris.logger.logLevels.WARNING == 30
        and eris.logger.logLevels.ERROR == 40
        and eris.logger.logLevels.CRITICAL == 50
    )
