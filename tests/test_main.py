#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# tests/test_main.py
## a component of eRis
* handles testing of eris/__main__.py
"""

# Standard Library Imports #
import asyncio
import signal

# Third-Party Imports #
import pytest

# First-Party Imports #
import eris
import eris.bot
import eris.logger


@pytest.mark.asyncio
async def test_bot_main(bot: eris.bot.Bot):
    assert bot.connTask is not None


@pytest.mark.asyncio
async def test_bot_task_exception_handler(bot: eris.bot.Bot):
    bot.handler.taskExceptionHandler(
        eris=bot, loop=bot.loop, ctx={"exception": Exception(), "message": "This is a test."}
    )


@pytest.mark.asyncio
async def test_bot_exception_handler(bot: eris.bot.Bot):
    async def _():
        raise NotImplementedError()

    fail_task = bot.loop.create_task(_())
    await asyncio.sleep(1)
    bot.handler.botExceptionHandler(eris=bot, eTask=fail_task)


@pytest.mark.asyncio
async def test_bot_stack_dumper(bot: eris.bot.Bot):
    eris.__main__.stackDumper(log=bot.log, sig=signal.SIGSEGV)


@pytest.mark.asyncio
async def test_bot_print_version_output(bot: eris.bot.Bot):
    await eris.__main__.print_version_output(log=bot.log)
