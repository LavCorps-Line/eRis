# eRis Discord Bot Core

## LavCorps-Line

To install eRis, simply install it from the LavCorps-Like PyPi Registry:

```shell
pip install eris --no-deps --index-url https://gitlab.com/api/v4/groups/6280387/-/packages/pypi/simple # Install eRis from LavCorps-Line PyPi
pip install eris # Fetch dependencies from PyPi
```

To update eRis, use the following:

```shell
pip install --force-reinstall eris --no-deps --index-url https://gitlab.com/api/v4/groups/6280387/-/packages/pypi/simple # Install eRis from LavCorps-Line PyPi
pip install eris # Fetch dependencies from PyPi
```

For more information, click [here](docs/index.md)