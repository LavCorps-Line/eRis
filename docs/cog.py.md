# eRis Cog

[Return to Index.](index.md)

`eRis Cogs` are modular pieces of code, loaded and unloaded into the eRis Core
at runtime. Commands, listeners, and other things can be defined in a Cog, all
contained within a Cog Class.

## Cog Metadata & Tips

Cog metadata is largely defined within [data.toml](data.toml.md), however there
are a few things controllable by the Cog in code.

* Cogs are free to set class attributes
* Cogs are free to be any class, as long as they inherit from `eris.cog.Cog`

Additionally:

* Cogs are free to access any public utility functions provided by the bot core
* Cogs are free to access other cogs by name via `Bot.auxCogs` or `Bot.coreCogs`
  , but note that cogs that are not defined as dependencies may not be loaded at
  the time a cog makes a call to another call.

## Cog Structure

There are four main parts to a Cog:

* The `__init__.py` data function
    <!-- TODO: document custom loading/unloading functionality here -->
    ```python
    from eris.cog import CogImportData  # Necessary import to return needed data
    from .example import example  # Necessary local cog import
    def data(**kwargs) -> CogImportData:  # Several kwargs are passed to data(), so it's typically easier to catch them all as **kwargs
        return CogImportData(cog=example(**kwargs))  # It's also easiest to just pass all kwargs into CogImportData as it comes
    ```
* The class `__init__` function
    ```python
    def __init__(self, **kwargs) -> None:
        self.bot: eris.bot.Bot = kwargs["bot"]  # It's good practice to provide a reference to the running bot in the Cog.
        self.dependency = kwargs["deps"]["dependency"]  # Don't forget to make a reference to your dependencies!
        self.some_attribute = True  # You can also store arbitrary attributes here.
    ``` 
* The built-in `__late_init__` function
    ```python
    async def __late_init__(self):  # This is scheduled to run shortly after our cog is loaded by `loader`.
        log.info("Doing late initialization stuff...")  # It will not run immediately after the cog is loaded,
        await self.late_initialization_stuff()  # so there is a slim chance this cog will be used before late initialization is done.
    ```
* The built-in `__cleanup__` function
    ```python
    async def __cleanup__(self):  # This is called when our cog is unloaded by `loader`.
        log.info("Cleaning up our trash...")
        await self.clean_up_our_trash()
    ```

## Cog Commands & Listeners

For Cog Commands & Listeners, check out the API reference
for [discord.ext.commands](https://discordpy.readthedocs.io/en/latest/ext/commands/api.html)
