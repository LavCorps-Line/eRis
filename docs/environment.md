# environment variables

[Return to Index.](index.md)

Due to the modular nature of eRis, we do not have access to persistent
configuration when we first start up. To circumvent this, important
configuration data related to config database authentication, discord token,
etc. are stored as environment variables.

Variables without a set default must be set before launching eRis Supervisor.
Default variables are only applied if eRis is run through eRis Supervisor.

## environment variable reference

| variable name          | purpose                                                                                                                                                            | default        |
|------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------|
| ERIS_DPY_LOGGING_LEVEL | Logging level for discord.py, see [here](https://docs.python.org/3.10/library/logging.html?highlight=logging#levels) for a list of valid logging levels.           | WARN           |
| ERIS_LOOP_TYPE         | Loop type used for eRis and eRis Supervisor, choice between `asyncio` and `uvloop`. `uvloop` is recommended for performance reasons.                               | asyncio        |
| ERIS_DISCORD_TOKEN     | Token used to authenticate the bot with Discord.                                                                                                                   | `None`         |
| ERIS_DISCORD_PREFIX    | Prefix used to communicate with the bot on Discord.                                                                                                                | `Empty String` |
| ERIS_LOGGING_LEVEL     | Logging level used for eRis components, see [here](https://docs.python.org/3.10/library/logging.html?highlight=logging#levels) for a list of valid logging levels. | WARNING        |
| ERIS_LOGGING_DIR       | Logging dir used for eRis and discord.py logging output, not automatically rotated but can be safely used in conjunction with tools such as logrotate.             | null           |
| ERIS_CONFIG_TYPE       | Type of config backend to use in eRis. Valid choices are `PSQL` and `SQLITE`.                                                                                      | `None`         |
| ERIS_PSQL_DBNAME       | PostgreSQL Database name to connect to. Doesn't need to be set if using SQLite3 backend.                                                                           | `Empty String` |
| ERIS_PSQL_USERNAME     | PostgreSQL Username to connect with. Doesn't need to be set if using SQLite3 backend.                                                                              | eris           |
| ERIS_PSQL_PASSWORD     | PostgreSQL Password to connect with. Doesn't need to be set if using SQLite3 backend.                                                                              | `None`         |
| ERIS_PSQL_HOST         | IP Address of Database Host to connect to. Doesn't need to be set if using SQLite3 backend.                                                                        | localhost      |
| ERIS_PSQL_PORT         | Port of Database Host to connect to. Doesn't need to be set if using SQLite3 backend.                                                                              | 5432           |
| ERIS_PSQL_TABLE        | Table of Database to connect to. Created automatically if needed and permissions are given. Doesn't need to be set if using SQLite3 backend.                       | eris           |
| ERIS_PSQL_SCHEMA       | Schema of Database to connect to. Created automatically if needed and permissions are given. Doesn't need to be set if using SQLite3 backend.                      | eris           |
| ERIS_SQLITE_DBPATH     | Path to SQLite3 database to connect to. Created automatically if needed and permissions are given. Doesn't need to be set if using PostgreSQL backend.             | `None`         |
| ERIS_SQLITE_TABLE      | Table of Database to connect to. Created automatically if needed and permissions are given. Doesn't need to be set if using PostgreSQL backend.                    | eris           |
| ERIS_RESTART_ON_CRASH  | Whether or not the eRis Supervisor should restart eRis on crash. Valid values are `True` and `False`.                                                              | False          |
| ERIS_USER              | Username for eRis Supervisor to run as                                                                                                                             | root           |
| ERIS_GROUP             | Group name for eRis Supervisor to run as                                                                                                                           | root           |

## file reference

The config file should be a standard toml file, laid out like so:

```toml
ERIS_DPY_LOGGING_LEVEL = "INFO"
ERIS_LOOP_TYPE = "uvloop"
ERIS_DISCORD_TOKEN = "79b34033d5463655e135ba67d35b9beb697096a9c6e1b4f81a46ad18b38c96f9"
ERIS_DISCORD_PREFIX = "$"
ERIS_LOGGING_LEVEL = "INFO"
ERIS_LOGGING_DIR = "/var/log/eRis"
ERIS_CONFIG_TYPE = "PSQL"
ERIS_PSQL_DBNAME = "eRisData"
ERIS_PSQL_USERNAME = "ellysion"
ERIS_PSQL_PASSWORD = "SuperSecurePassword"
ERIS_PSQL_HOST = "10.39.39.39"
ERIS_PSQL_PORT = "3939"
ERIS_PSQL_TABLE = "eris"
ERIS_PSQL_SCHEMA = "public"
ERIS_RESTART_ON_CRASH = "True"
ERIS_USER = "ellysion"
ERIS_GROUP = "nogroup"
```

Keep in mind that variables from the file will NOT overwrite the environment
variables eRis Supervisor is loaded with.

The following is a list of valid file paths for the eRis config files.

* `/etc/eris`

Keep in mind that this is listed in order of access. Later files will overwrite
variables from earlier files.