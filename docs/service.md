# eRis as an Init Service

[Return to Index.](index.md)

Instructions on how to make eRis work as a systemd/OpenRC service

## OpenRC

Due to the relative simplicity of OpenRC, an eRis service file is rather light.
The only noteworthy caveats are as follows:

* `command_background` must be set to `true`, `yes`, or `1`
* as a requirement of setting `command_background`, `pidfile` must be set.
  recommended value is `pidfile="/run/${RC_SVCNAME}.pid"`

An example OpenRC Service File for eRis can be found [here](services/openrc).

## systemd

A systemd service file for eRis is a bit heavier than OpenRC, but still none too
difficult to figure out. The only noteworthy caveats are as follows:

* eRis outputs all logging to the given logging directory as well as stdout.
  consider setting file logging to `null` and using the systemd journal instead.
* eRis Supervisor will handle restarts on request (and crashes, if set), making
  systemd's restart handler unnecessary.
* eRis will respond to any of `SIGTERM`, `SIGINT`, and `SIGQUIT` to cleanly
  stop.
* eRis will respond to any of `SIGHUP`, and `SIGUSR1` to re-sync the discord.py
  command tree.

An example systemd Service File for eRis can be
found [here](services/systemd.service)

## Platform independent information

* Unless you install eRis to the system Python installation (not recommended),
  eRis or eSupervisor must be called from inside the relevant venv.
    * Consider using a startup script to automatically enter the venv, export
      any environment variables specific to that eRis service, and
      finally `exec eSupervisor`.