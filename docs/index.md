# Documentation Index

## eRis as a System Service

* [eRis as a System Service Guide](service.md)
* [eRis Environment Variables Guide](environment.md)
* [Example OpenRC Service File](services/openrc)
* [Example systemd Service File](services/systemd.service)

## eRis Cog Development Reference

* [Cog Development Guide](cog.py.md)
* [Cog Metadata Guide](data.toml.md)
* [Cog libConfig Usage Guide](libConfig.md)