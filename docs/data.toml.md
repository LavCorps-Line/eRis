# data.toml reference

[Return to Index.](index.md)

`data.toml` provides [Cog](cog.py.md) metadata, notably cog dependencies and
provided classes.

Can be accessed in code via `Cog.__data__`

* `[name]`
    * Machine-readable name for the cog.
    * Should be all lowercase, alphanumeric.
    * Should be reasonably unique (for the sake of `depends`).
    * Consider keeping this name consistent with the "main" class of the cog.
* `name`
    * Human-readable name for the cog.
    * Defaults to the machine-readable name
* `description`
    * Long human-readable description of the cog.
    * Defaults to the cog's docstring as defined in `__init__.py`, or an empty
      string if unset.
* `version`
    * Version number of cogs, preferably
      in [semantic versioning](https://en.wikipedia.org/wiki/Software_versioning#Semantic_versioning)
      .
        * Make sure to explicitly declare the version as
          a [`String`](https://toml.io/en/v1.0.0#string)
    * Defaults to `0.0.0`
* `depends`
    * Cog dependencies, referring to class names.
    * Should be an [`Array`](https://toml.io/en/v1.0.0#array).
        * Defaults to an empty `Array`.
* `path`
    * Path to cog's `__init__.py`, relative to `data.toml`.
        * Defaults to the given
          machine-readable `{data.toml parent dir}/{name}/__init__.py`.
