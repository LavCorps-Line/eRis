# libConfig reference

[Return to Index.](index.md)

`libConfig` is the eRis library for persistent configuration/data storage.

* To ensure `libConfig` is usable in your cog, simply state `libConfig` as a
  dependency in your cog's `data.toml`.
* To use `libConfig` in your cog:
    * Import the `lockManager` class with
      `from eris.cogs.libConfig.lockman import lockManager`
    * Use the `async with lockManager(snf, bot, cog) as data` async context
      manager.
        * `snf` can only be a `discord.abc.Snowflake`.
            * Consider passing ctx.guild, ctx.author, or ctx.channel as snf!
        * `cog` can only be an `eris.cog.Cog`.
            * For use inside a command, simply pass `self`!
            * If you need to use libConfig from outside a cog, refer to the
              legacy instructions.
        * `bot` can only be an `eris.bot.Bot`.
            * Consider saving the `bot` kwarg in cog init and passing
              `self.bot`!
        * `data` points to a dict initialized within the `lockManager` class.
            * Be careful to avoid reassigning this variable!
            * `data` within the `lockManager` will be used to transparently save
              your modified data.
            * The only valid type for `data` is a `dict`. `data` must be able to
              be serialized to JSON.
            * `data` examples:
                ```json
                {
                    "secretValue":"ba1d24705dd2841da238315b9045806ecf595a17e9aea27285cb73fdb54d66ca",
                    "guid":"34385b3c-d6ac-4605-993b-6a97b516fc57",
                    "randomJson":[
                        {},
                        {}
                    ]
                }
                ```
                ```json
                {
                    "users": [
                        {
                            "username":"kadam0",
                            "favoriteAnimal":"Gull, southern black-backed"
                        },
                        {
                            "username":"rgillhespy1",
                            "favoriteAnimal":"Little grebe"
                        }
                    ]
                }
                ```
* Database layout:
    * PostgreSQL:
        * ctx_snowflake = bigint
        * cog = text
        * json_data = jsonb
    * SQLite3:
        * ctx_snowflake = integer
        * cog = text
        * json_data = text

## LEGACY INSTRUCTIONS

* To access `libConfig` in your cog:
    1. state `libConfig` as a dependency in your cog's `data.toml`
    2. `loader` will pass the dependencies requested inside the `deps` kwarg
       when calling the relevant load function. If you use the default load
       function, as well as pass kwargs in your cog's data() function to
       CogImportData, you don't have to worry about this.
    3. in your cog's `__init__.py`, retrieve `deps`.`libConfig` from the kwargs:
        ```python
        import eris.bot
        import eris.cog
        from eris.cogs.libConfig import libConfig
        class libEvent(eris.cog.Cog):
            def __init__(self, **kwargs) -> None:
                self.bot: eris.bot.Bot = kwargs["bot"]
                self.config: libConfig = kwargs["deps"]["libConfig"]
        ``` 
* to use `libConfig` in your cog:
    * use `libConfig.read(snf, cog)` to read data from the
      config database
        * `snf` can only be an integer, or a string interpretable as an integer
        * `cog` can be whatever is desired, but consider keeping it as unique as
          your cog's machine-readable name, preferably even using your cog's
          machine-readable name.
    * use `libConfig.write(snf, cog, data)`
        * mostly the same arguments as before, with the exception of...
        * `data` can be whatever valid JSON data you want. it's recommended
          to use only data that can be stored in JSON format here, as this
          argument will be processed directly by `json.dumps()`
        * `data` examples:
            ```json
            {
                "secretValue":"ba1d24705dd2841da238315b9045806ecf595a17e9aea27285cb73fdb54d66ca",
                "guid":"34385b3c-d6ac-4605-993b-6a97b516fc57",
                "randomJson":[
                    {},
                    {}
                ]
            }
            ```
            ```json
            [
                {
                    "username":"kadam0",
                    "favoriteAnimal":"Gull, southern black-backed"
                },
                {
                    "username":"rgillhespy1",
                    "favoriteAnimal":"Little grebe"
                }
            ]
            ```
* Database layout:
    * PostgreSQL:
        * ctx_snowflake = bigint
        * cog = text
        * json_data = jsonb
    * SQLite3:
        * ctx_snowflake = integer
        * cog = text
        * json_data = text