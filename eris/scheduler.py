#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# /scheduler.py
## a component of eRis
* scheduler logic for the bot
"""

# Standard Library Imports #
import asyncio
import heapq
from typing import Coroutine

# First-Party Imports #
import eris.bot
from eris.logger import logger


class ScheduleObject:
    """Simple data container intended to store data inside a ScheduleQueue"""

    def __init__(self, **kwargs):
        self.time: float = kwargs["time"] if "time" in kwargs else 0.0
        self.name: str = kwargs["name"] if "name" in kwargs else None
        self.coro = kwargs["coro"] if "coro" in kwargs else None
        self.callbacks: list = kwargs["callbacks"] if "callbacks" in kwargs else []
        self.default_callback: bool = (
            kwargs["default_callback"] if "default_callback" in kwargs else False
        )


class ScheduleQueue(asyncio.Queue):
    """A subclass of asyncio.Queue; entries must be of type ScheduleObject, retrieved by lowest priority score."""

    def _init(self, maxsize):
        self._queue: list[tuple[int | float, ScheduleObject]] = []

    def _put(self, item: ScheduleObject) -> None:
        if not isinstance(item, ScheduleObject):
            raise TypeError(f"Expected ScheduleObject, got {type(item)}!")
        heapq.heappush(self._queue, (item.time, item))

    def _get(self) -> ScheduleObject:
        return heapq.heappop(self._queue)[1]


class Scheduler:
    """
    eRis Scheduler, a component of eRis
    """

    def __init__(self):
        self.bot: eris.bot.Bot | None = None
        self.log = logger("eRis.Scheduler")
        self.sched_worker: asyncio.Task | None = None
        self.operating: bool = False
        self._flushing: bool = False
        self.schedule: ScheduleQueue = ScheduleQueue()
        self.running_scheduled_tasks = set()

    def _scheduled_task_callback(self, task: asyncio.Future):
        """internal task callback cleanup for eRis scheduled tasks

        Parameters
        ----------
        task: `asyncio.Future`
        """
        try:
            task.result()
        except asyncio.CancelledError as caEr:
            self.log.exception("A scheduled task was cancelled!", exc_info=caEr)
        except asyncio.InvalidStateError as inStEr:
            self.log.exception(
                "A scheduled task triggered callback but isn't finished!", exc_info=inStEr
            )
        except Exception as ex:
            self.log.exception("A scheduled task had an unhandled exception!", exc_info=ex)

    def _spawn_task(self, task: ScheduleObject) -> None:
        future: asyncio.Task = self.bot.loop.create_task(task.coro, name=task.name)
        self.log.debug(f"spawning task: {task.name}")
        for callback in task.callbacks:
            future.add_done_callback(callback)
        if task.default_callback:
            self.running_scheduled_tasks.add(future)
            future.add_done_callback(self._scheduled_task_callback)
        future.add_done_callback(self.running_scheduled_tasks.discard)

    async def start(self) -> asyncio.Task:
        """
        Start up and return the task of the scheduler worker

        Returns
        -------
        asyncio.Task
        """
        self.log.debug("starting scheduler")
        if self.operating:
            raise RuntimeError("Can't start an already-running scheduler!")
        if self._flushing:
            raise RuntimeError("Cowardly refusing to start while scheduler is being flushed!")
        else:
            self.sched_worker = self.bot.loop.create_task(
                self.schedule_worker(), name="eRisScheduleWorker"
            )
            self.operating = True
            return self.sched_worker

    async def stop(self) -> asyncio.Future:
        """
        Stop the scheduler, and return the result of our graceful stop. May raise asyncio.TimeoutError

        Returns
        -------
        asyncio.Future
        """
        self.log.debug("starting scheduler")
        if not self.operating:
            raise RuntimeError("Can't stop an already-stopped scheduler!")
        else:
            self.operating = False
            return await asyncio.wait_for(self.sched_worker, 5)

    async def flush(self, timeout: float | int = 5.0) -> None:
        """
        Flush out the schedule.

        Parameters
        ----------
        timeout: `float` | `int`
            defaults to 5.0
        """
        if self.operating:
            self.log.warn(
                "Cowardly refusing to flush scheduler while there's a running schedule worker..."
            )
            return
        if self._flushing:
            self.log.warn(
                "Cowardly refusing to flush scheduler while already flushing the scheduler..."
            )
            return
        if isinstance(timeout, int):
            timeout = float(timeout)
        _timeout = timeout
        timeout += self.bot.loop.time()
        self.log.warn("Flushing scheduler, expect some weirdness...")
        self._flushing = True
        while not self.schedule.empty():
            task = await self.schedule.get()
            self._spawn_task(task)
            self.schedule.task_done()
        while len(self.running_scheduled_tasks):
            if self.bot.loop.time() >= timeout:
                break
            elif self.bot.loop.time() + (_timeout / 2) >= timeout:
                task = self.running_scheduled_tasks.pop()
                task.cancel()
                await asyncio.gather(task, return_exceptions=True)
            await asyncio.sleep(0)
        self._flushing = False

    async def schedule_worker(self) -> None:
        """Perpetually running schedule worker"""
        self.log.info("Schedule worker created!")
        while self.operating:
            try:
                task: ScheduleObject = await asyncio.wait_for(
                    self.schedule.get(), 2.5
                )  # Wrap this in asyncio.wait_for() so we can gracefully shut down while schedule is inactive
            except asyncio.TimeoutError:
                continue
            else:
                if (
                    self.bot.loop.time() <= task.time
                ):  # If it's not time for the scheduled task to be executed...
                    await self.schedule.put(task)  # Put it back :)
                else:  # Otherwise...
                    self._spawn_task(task)  # Spawn our task and put it to work
                self.schedule.task_done()  # Mark our removed task from the queue as done to prevent unloading errors

    def schedule_task(
        self,
        coro: Coroutine,
        offset: float | int = 0.0,
        name: str | None = None,
        callbacks: list | None = None,
        default_callback: bool = True,
    ) -> None:
        """
        Parameters
        ----------
        coro: `coroutine`
        offset: `float` or `int`
            how many seconds into the future should the task happen?
            defaults to `0.0`
        name: `str` or `None`
            defaults to `None`
        callbacks: `list`
            list of functions that should be attached as a callback
            see add_done_callback() documentation for asyncio for more details
        default_callback: `bool`
            whether to allow the default callback to be added, defaults to `True`
        """
        if not callbacks:
            callbacks = []
        self.log.debug(f"scheduling coroutine {coro} to run after {offset}")
        if isinstance(offset, int):
            offset = float(offset)
        offset += self.bot.loop.time()
        self.schedule.put_nowait(
            ScheduleObject(
                time=offset,
                coro=coro,
                name=name if name else f"{coro.__name__} {id(coro)}",
                callbacks=callbacks,
                default_callback=default_callback,
            )
        )
