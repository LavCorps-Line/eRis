#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# /shared.py
## a component of eRis
* shared logic for the bot
"""

# Standard Library Imports #
from enum import IntEnum


class exit_codes(IntEnum):
    """IntEnumerator of exit codes"""

    error = 1
    shutdown = 0
    restart = 39


class shutdown_causes(IntEnum):
    """IntEnumerator of shutdown causes"""

    user_requested_stop = 0
    user_requested_restart = 1
    unhandled_exception = 2
    caught_signal = 3
