#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# /lib/logger.py
## a component of eRis
* standardized formatting for eRis logging
"""

# Standard Library Imports #
import datetime
import inspect
import io
import logging
import logging.handlers
import os
import pathlib
import sys
import traceback
import types
import typing
from enum import IntEnum


class logger:
    """
    lib.logger, a component of eRis
    """

    def __init__(self, name: str):
        # Initializing needed classes for logging
        _initErrors = []
        if "ERIS_LOGGING_LEVEL" in os.environ:
            if os.environ["ERIS_LOGGING_LEVEL"].isdigit():
                try:
                    logLevels(int(os.environ["ERIS_LOGGING_LEVEL"]))
                except KeyError as keyErr:
                    _initErrors.append(keyErr)
                    self._logLevel: logLevels = logLevels.DEBUG
                else:
                    self._logLevel: logLevels = logLevels(int(os.environ["ERIS_LOGGING_LEVEL"]))
            else:
                try:
                    logLevels.__getitem__(os.environ["ERIS_LOGGING_LEVEL"])
                except KeyError as keyErr:
                    _initErrors.append(keyErr)
                    self._logLevel: logLevels = logLevels.DEBUG
                else:
                    self._logLevel: logLevels = logLevels.__getitem__(
                        os.environ["ERIS_LOGGING_LEVEL"]
                    )
        else:
            _initErrors.append(KeyError("ERIS_LOGGING_LEVEL environment variable is undefined!!"))
            self._logLevel: logLevels = logLevels.DEBUG
        self._logger = logging.getLogger(name)
        self._logDir = (
            os.environ["ERIS_LOGGING_DIR"] if "ERIS_LOGGING_DIR" in os.environ else "null"
        )
        if self._logDir and self._logDir != "null":
            os.makedirs(self._logDir, exist_ok=True)
            if not os.path.exists(self._logDir):
                _initErrors.append(
                    RuntimeError(
                        """
                        Couldn't create our logging directory for some reason!
                        Do we have the necessary permissions? Create it yourself if not!
                        """
                    )
                )
                self._handler = logging.NullHandler()
            elif not os.access(self._logDir, os.W_OK | os.X_OK):
                _initErrors.append(
                    RuntimeError(
                        """
                        We don't have permission to write to our logging directory!
                        """
                    )
                )
                self._handler = logging.NullHandler()
            else:
                self._handler = logging.handlers.WatchedFileHandler(
                    filename=pathlib.Path(f"{self._logDir}/{name}").resolve(),
                    encoding="UTF-8",
                )
        else:
            self._handler = logging.NullHandler()
        self._stdOutHandler = logging.StreamHandler(stream=sys.stdout)
        self._stdErrHandler = logging.StreamHandler(stream=sys.stderr)
        self._formatter = eRisFormatter()
        # Setting up needed classes
        self._logger.propagate = False
        self._logger.setLevel(logLevels.DEBUG)
        self._logger.addHandler(self._handler)
        self._logger.addHandler(self._stdOutHandler)
        self._handler.setFormatter(self._formatter)
        self._stdOutHandler.setFormatter(self._formatter)
        self._handler.setLevel(self._logLevel)
        self._stdOutHandler.setLevel(self._logLevel)
        self._stdErrHandler.setLevel(logLevels.ERROR)
        # Passthroughs to self._logger
        self.debug = self._logger.debug
        self.info = self._logger.info
        self.warn = self._logger.warning
        self.warning = self._logger.warning
        self.error = self._logger.error
        self.exception = self._logger.exception
        self.critical = self._logger.critical
        self.getChild = self._logger.getChild
        self.getEffectiveLevel = self._logger.getEffectiveLevel
        self.isEnabledFor = self._logger.isEnabledFor
        self.addFilter = self._logger.addFilter
        self.removeFilter = self._logger.removeFilter
        self.filter = self._logger.filter
        self.addHandler = self._logger.addHandler
        self.removeHandler = self._logger.removeHandler
        # Passthroughs to self._formatter
        self.format = self._formatter.format
        self.formatTime = self._formatter.formatTime
        self.formatException = self._formatter.formatException
        self.formatStack = self._formatter.formatStack
        while len(_initErrors) != 0:
            self.exception(
                "Exception occurred on logger initialization!", exc_info=_initErrors.pop()
            )

    def setLevel(self, level: int | str = 10) -> None:
        """
        Update the logging level

        Parameters
        ----------
        level: `int | str`
            This refers to the output threshold level, see logLevels
        """
        if isinstance(level, str):
            try:
                logLevels.__getitem__(level)
            except KeyError:
                raise ValueError("setLevel called with invalid `level` arg!")
            else:
                try:
                    self._logLevel = logLevels(logLevels.__getitem__(level))
                except ValueError:
                    raise ValueError("setLevel called with invalid `level` arg!")
        else:
            try:
                self._logLevel = logLevels(level)
            except ValueError:
                raise ValueError("setLevel called with invalid `level` arg!")

        self._handler.setLevel(self._logLevel)
        self._stdOutHandler.setLevel(self._logLevel)


class eRisFormatter(logging.Formatter):
    """Custom eRis Log Formatter"""

    def __init__(self):
        super().__init__(
            fmt="{isotime} @ {name}[{levelname}]: {message}", datefmt="", style="{", validate=False
        )
        self.fmt = "{isotime} @ {name}[{levelname}]: {message}"

    # TODO: if a message has newlines, break it up so all message newlines are prefixed by the non-message format
    def format(
        self, record: logging.LogRecord
    ) -> str:  # I feel like we can optimize this further. Ask Sina about it.
        """Format a LogRecord, bringing together a message, exception info, and stack info.

        Parameters
        ----------
        record: `logging.LogRecord`

        Returns
        -------
        `str`
        """
        message = self.formatMessage(record)
        if record.exc_info:
            if not record.exc_text:
                record.exc_text = self.formatException(record.exc_info)
        if record.exc_text:
            if message[-1:] != "\n":
                message = message + "\n"
            message = message + record.exc_text
        if record.stack_info:
            if message[-1:] != "\n":
                message = message + "\n"
            message = message + record.stack_info
        return message

    def formatTime(self, record: logging.LogRecord, datefmt: str = "") -> str:
        """Format a LogRecord, returning an ISO-8601 compliant timestamp as a string

        Parameters
        ----------
        record: `logging.LogRecord`
        datefmt: `str`
            Deprecated, included only for compatibility

        Returns
        -------
        `str`
        """
        return datetime.datetime.fromtimestamp(
            record.created, tz=datetime.datetime.now(datetime.timezone.utc).astimezone().tzinfo
        ).isoformat()

    def formatMessage(self, record: logging.LogRecord) -> str:
        """Format a LogRecord according to the template given when the formatter was initialized.

        Parameters
        ----------
        record: `logging.LogRecord`
        """
        data = record.__dict__
        data["isotime"] = self.formatTime(record)
        data["message"] = data["msg"]
        message = self.fmt.format(**data)
        message = message.replace("{", "{{").replace("}", "}}")
        message = message.replace("%s", "{}").format(*data["args"])
        message = message.replace("{{", "{").replace("}}", "}")
        return inspect.cleandoc(message)

    # def formatException(self, exceptionInfo:  -> str:
    def formatException(
        self,
        exceptionInfo: typing.Union[
            tuple[type(BaseException), BaseException, typing.Optional[types.TracebackType]],
            tuple[None, None, None],
        ],
    ) -> str:
        """Format a LogRecord, returning a string with information from a given exception

        Parameters
        ----------
        exceptionInfo: `tuple[type(BaseException), BaseException, typing.Optional[types.TracebackType]]`
        """
        with io.StringIO() as tracebackPrint:
            traceback.print_exception(
                exceptionInfo[0], exceptionInfo[1], exceptionInfo[2], None, tracebackPrint
            )
            tracebackOutput = tracebackPrint.getvalue()
        if tracebackOutput[-1:] == "\n":
            tracebackOutput = tracebackOutput[:-1]
        return tracebackOutput

    def formatStack(self, stackInfo: str) -> str:
        """Format a LogRecord, returning a string with stack info in it

        Parameters
        ----------
        stackInfo: `str`

        Returns
        -------
        `str`
        """
        return stackInfo


class logLevels(IntEnum):
    """IntEnumerator of log levels for logging"""

    NOTSET = 0
    DEBUG = 10
    INFO = 20
    WARNING = 30
    ERROR = 40
    CRITICAL = 50
