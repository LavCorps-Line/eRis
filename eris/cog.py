#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# /cog.py
## a component of eRis
* cog definitions for the bot
"""

# Standard Library Imports #
import pathlib
from typing import Any, Callable, Generator

# Third-Party Imports #
import discord.ext.commands
import toml


class CogData:
    """eRis Cog Data, a component of eRis"""

    def __init__(self, **kwargs):
        self.name: str = kwargs["name"]
        self.human_name: str = kwargs["human_name"] if "human_name" in kwargs else kwargs["name"]
        self.description: str = kwargs["description"] if "descriptions" in kwargs else ""
        self.version: str = kwargs["version"] if "version" in kwargs else "0.0.0"
        self.depends: list[str] = kwargs["depends"] if "depends" in kwargs else []
        self.data_path: str = str(pathlib.Path(kwargs["data_path"]).resolve())
        self.init_path: str = (
            str(pathlib.Path(kwargs["path"]).resolve())
            if "path" in kwargs
            else str(pathlib.Path(kwargs["loader_path"]).resolve())
        )
        self.priority: int = kwargs["priority"] if "priority" in kwargs else len(self.depends)
        self.load: Callable[..., Any] | None = None
        self.unload: Callable[..., Any] | None = None

    @classmethod
    def __import__(cls, path: str | pathlib.Path) -> Generator:
        with open(path) as tmpFile:
            try:
                tmpToml = toml.load(tmpFile)
            except toml.TomlDecodeError:
                raise ImportError("Core Cog data.toml is malformed!")

        for cogDef in tmpToml:
            tmpCogDef = {}
            for cDef in tmpToml[cogDef]:
                if cDef == "path":
                    tmpCogDef["init_path"] = tmpToml[cogDef][cDef]
                if cDef == "name":
                    tmpCogDef["human_name"] = tmpToml[cogDef][cDef]
                else:
                    tmpCogDef[cDef] = tmpToml[cogDef][cDef]
            tmpCogDef["name"] = cogDef
            tmpCogDef["data_path"] = path
            tmpCogDef["loader_path"] = (
                pathlib.Path(path)
                if isinstance(path, str)
                else path / ".." / tmpCogDef["name"] / "__init__.py"
            ).resolve()
            yield cls(**tmpCogDef)


class CogImportData:
    """eRis Cog Import Data, a component of eRis"""

    def __init__(self, **kwargs):
        self.cog: Cog = kwargs["cog"]
        self.custom_load: Callable[..., Any] | None = (
            kwargs["loader"] if "loader" in kwargs else None
        )
        self.custom_unload: Callable[..., Any] | None = (
            kwargs["unloader"] if "unloader" in kwargs else None
        )


class Cog(discord.ext.commands.Cog):
    """eRis Cog Class, a component of eRis"""

    __data__: CogData | None

    def __new__(cls, *args, **kwargs):
        self = super().__new__(cls)
        # noinspection PyTypeHints
        self.__data__: CogData | None = None
        return self

    async def __late_init__(self) -> None:
        """Stub late init function"""
        pass

    async def __cleanup__(self) -> None:
        """Stub cleanup function"""
        pass
