#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# /lib/handler.py
## a component of eRis
* shutdown/exception handling
* provides exit codes
"""

# Standard Library Imports #
import asyncio
import os
import sys

# First-Party Imports #
from eris.bot import Bot
from eris.logger import logger
from eris.shared import exit_codes, shutdown_causes


class handler:
    """
    lib.handler, a component of eRis
    * handles exceptions and shutdowns
    """

    def __init__(self) -> None:
        self.exitCodes = exit_codes
        self.shutdownCauses = shutdown_causes
        self.shutdownTask: asyncio.Task | None = None
        self.log = logger("eRis.core.lib.handler")
        self._closing = False

    async def shutdownHandler(self, eris: Bot, **kwargs) -> None:
        """
        Initiates clean shutdown process

        Parameters
        ----------
        kwargs
        eris: `eris.bot.Bot`
        """
        if not self._closing:
            self._closing = True  # Don't allow shutdownHandler to be called repeatedly
        else:
            return

        if "cause" not in kwargs:
            self.log.warn("Submitting to shutdown request of unknown origin.")
            eris.exitCode = self.exitCodes.error
        else:
            cause: int = kwargs["cause"]
            if cause == 0:
                self.log.warn("Submitting to user requested shutdown.")
                eris.exitCode = self.exitCodes.shutdown
            elif cause == 1:
                self.log.warn("Submitting to user requested restart.")
                eris.exitCode = self.exitCodes.restart
            elif cause == 2:
                self.log.exception(
                    "Caught unhandled exception. Shutting down...", exc_info=kwargs["exception"]
                )
                eris.exitCode = self.exitCodes.error
            elif cause == 3:
                self.log.warn(f"Caught {kwargs['signal'].name}. Shutting down...")
                eris.exitCode = self.exitCodes.shutdown

        cogs = [cog for cog in eris.coreCogs]  # Initialize our cogs list
        cogs.sort(key=lambda c: eris.coreCogs[c].__data__.priority)  # Sort our cogs list
        iterations = 0  # Init our iterations count
        maxIterations = len(eris.coreCogs) * 2  # Init our maxIterations count
        while len(cogs) != 0:  # While there are cogs waiting to be unloaded...
            iterations += 1  # Increase our iterations count
            if iterations >= maxIterations:
                raise RecursionError(  # Throw an error if we've gone around this loop too many times
                    f"""
                    Iteration count {iterations} exceeds allowance!
                    We're failing to unload a core cog's dependents!
                    """
                )
            cogDepData = eris.dependencies_dict(core=True)
            cog = cogs.pop()  # Remove cog from cogs list, returning highest priority cog first.
            if cogDepData[cog]["dependents"]:  # If cog still has active dependents...
                cogs.append(cog)  # Put it back in the cog list
                continue
            self.log.debug(f"Cleaning up {cog}...")
            await eris.coreCogs[
                cog
            ].__cleanup__()  # Call cog ___cleanup__(), does nothing if not defined in the cog
            await eris.coreCogs[cog].__data__.unload(
                cog=eris.coreCogs[cog], core=True
            )  # Remove cog from eRis and discord.py
            sys.modules.pop(cog)  # Remove cog from sys.modules
        self.log.info("Core cogs cleaned up and unloaded.")

        if eris.scheduler.operating:
            self.log.info("Stopping scheduler...")
            schedStop = eris.loop.create_task(eris.scheduler.stop())
            try:
                await asyncio.wait_for(schedStop, 5.5)
            except asyncio.TimeoutError:
                self.log.info("Failed to gracefully stop scheduler.")
            else:
                try:
                    await asyncio.wait_for(eris.scheduler.flush(5), 5.5)
                except asyncio.TimeoutError:
                    self.log.info("Failed to gracefully flush scheduler.")

        closeTask: asyncio.Task | None = None
        try:
            if not eris.is_closed():
                closeTask = eris.loop.create_task(eris.close())
        finally:
            if "CI_JOB_NAME_SLUG" in os.environ and "test" in os.environ["CI_JOB_NAME_SLUG"]:
                pass
            else:
                pendingTasks = [
                    task
                    for task in asyncio.all_tasks()
                    if task is not asyncio.current_task() and task is not closeTask
                ]
                if len(pendingTasks) != 0:
                    self.log.info("Cancelling all other pending tasks...")
                    [
                        self.log.debug(str(task))  # Log task for debug purposes
                        for task in pendingTasks
                        if task
                        is not asyncio.current_task()  # Make sure it isn't the current task
                        and task is not closeTask  # or the closeTask
                    ]
                    [
                        task.cancel()  # Cancel task
                        for task in pendingTasks
                        if task is not asyncio.current_task() and task is not closeTask
                    ]
                    [
                        asyncio.gather(task, return_exceptions=True)  # Gather results & exceptions
                        for task in pendingTasks
                        if task is not asyncio.current_task() and task is not closeTask
                    ]
        if closeTask:  # If we do have a closeTask...
            timeout = eris.loop.time() + 5.0  # Set our timeout
            while not closeTask.done():
                if eris.loop.time() > timeout:  # If we're past our timeout, give up and cancel
                    self.log.warn("Couldn't close websocket within timeout, giving up...")
                    closeTask.cancel()
                else:
                    await asyncio.sleep(0)  # Otherwise, wait
            await asyncio.gather(closeTask, return_exceptions=True)

        await eris.httpClient.close()

        self.log.info("All tasks cancelled, bot closed, halting event loop...")
        if "CI_JOB_NAME_SLUG" in os.environ and "test" in os.environ["CI_JOB_NAME_SLUG"]:
            pass
        else:
            asyncio.get_event_loop().stop()  # Stop our event loop, returning to main() in __main__.py

    def taskExceptionHandler(self, eris: Bot, loop: asyncio.AbstractEventLoop, ctx: dict) -> None:
        """
        Handles exceptions in tasks

        Parameters
        ----------
        eris: `eris.bot.Bot`
        loop: `asyncio.AbstractEventLoop`
        ctx: `dict`
        """
        ex = ctx.get("exception")
        if isinstance(ex, (KeyboardInterrupt, SystemExit)):
            return
        self.log.critical(
            f"""
            Unhandled exception in {ctx["future"] if "future" in ctx else "N/A"} in {loop}!
            {ctx["message"]}
            """
        )
        self.log.warning("Attempting graceful shutdown...")
        self.shutdownTask = loop.create_task(
            coro=self.shutdownHandler(
                eris, cause=shutdown_causes.unhandled_exception, exception=ex
            )
        )

    def botExceptionHandler(self, eris: Bot, eTask: asyncio.Future) -> None:
        """
        Handles otherwise unhandled exceptions

        Parameters
        ----------
        eris: `eris.bot.Bot`
        eTask: `asyncio.Future`
        """
        try:
            eTask.result()
        except (SystemExit, KeyboardInterrupt, asyncio.CancelledError):
            pass
        except Exception as ex:
            self.log.critical("Main eRis task didn't handle an exception!")
            self.log.warning("Attempting graceful shutdown...")
            self.shutdownTask = eris.loop.create_task(
                self.shutdownHandler(eris, cause=shutdown_causes.unhandled_exception, exception=ex)
            )
