#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# /__main__.py
## a component of eRis
* handles initialization
* handles requirement checking
"""

# Standard Library Imports #
import asyncio
import functools
import io
import os
import pathlib
import signal
import sys

# Third-Party Imports #
import discord
import discord.ext.commands as commands

# First-Party Imports #
from eris.shared import exit_codes

if "ERIS_LOOP_TYPE" in os.environ and os.environ["ERIS_LOOP_TYPE"] == "uvloop":
    try:
        # Third-Party Imports #
        import uvloop
    except ImportError as impErr:
        print("Tried to import uvloop, but ImportError was raised! Are you sure it's installed?")
        raise impErr
elif "ERIS_LOOP_TYPE" not in os.environ:
    os.environ["ERIS_LOOP_TYPE"] = "asyncio"

# First-Party Imports #
import eris.exceptions as exceptions
import eris.logger
from eris.bot import Bot
from eris.cog import CogData
from eris.handler import handler as _handler
from eris.logger import logger


async def eRisInit(bot: Bot, log: eris.logger.logger) -> None:
    """
    Handles early cog loading

    Parameters
    ----------
    bot
    log
    """

    await bot.wait_until_ready()  # Wait until discord.py reports we're ready
    log.info("eRis is ready, loading core cogs...")

    coreCogDataPath = (pathlib.Path(__file__) / ".." / "cogs" / "data.toml").resolve()  # Hacky :/
    coreCogData: list[CogData] = [
        cogDef for cogDef in CogData.__import__(coreCogDataPath)
    ]  # Import cog data from data.toml

    iterations = 0  # Init our iterations count
    maxIterations = len(coreCogData) * 2  # Init our maxIterations count

    while len(coreCogData) != 0:  # While we haven't parsed all of coreCogData...
        iterations += 1  # Increase our iterations count
        if iterations >= maxIterations:
            raise RecursionError(  # Throw an error if we've gone around this loop too many times
                f"""
                Iteration count {iterations} exceeds allowance!
                A core cog is missing a dependency, or otherwise failing to load!
                """
            )
        cog: CogData = coreCogData.pop()  # Pop an instance of CogData out of coreCogData
        log.debug(f"Loading {cog.name} from {cog.init_path}...")
        try:
            await bot.__load_cog__(cog, core=True)
            await bot.coreCogs[cog.name].__late_init__()
        except exceptions.DependencyNotLoaded:
            coreCogData.insert(0, cog)
            continue
    log.info(
        f"Successfully loaded and initialized all core cogs after {iterations} of loading attempts!"
    )
    await bot.scheduler.start()  # Start up scheduler
    bot.scheduler.schedule_task(
        coro=bot.change_presence(), name="bot change presence"
    )  # update bot presence


async def eRisAuth(bot: Bot, log: eris.logger.logger) -> None:
    """
    Handles eRis Authentication

    Parameters
    ----------
    bot
    log
    """
    if "ERIS_DISCORD_TOKEN" in os.environ:
        token = os.environ["ERIS_DISCORD_TOKEN"]
    else:  # Make sure we got a token
        log.critical("Token must be set via environment to login!")
        sys.exit(1)
    try:
        log.info("Logging into discord...")
        await bot.login(token)
    except discord.LoginFailure as loFa:
        log.critical("Invalid token!", exc_info=loFa)
        sys.exit(1)
    except discord.PrivilegedIntentsRequired as prInRe:
        log.critical("Privileged intents are not enabled!", exc_info=prInRe)
        sys.exit(1)
    except discord.HTTPException as httpEx:
        log.critical(
            "Some kind of odd error occurred while attempting to log in!", exc_info=httpEx
        )
        sys.exit(1)


def stackDumper(log: eris.logger.logger, sig=None) -> None:
    """
    Dump stack of all running tasks

    Parameters
    ----------
    log
    sig
    """
    log.debug("Caught {sig.name}, dumping stacks from all running tasks...")
    strs = []
    for task in asyncio.all_tasks():
        strOut = io.StringIO()
        strs.append(strOut)
        task.print_stack(limit=99, file=strOut)
    for strOut in strs:
        log.debug(strOut.getvalue())
        strOut.close()


def environmentVerify() -> None:
    """Verify system environment"""
    minPythonVersion = (3, 10, 0)
    if sys.version_info < minPythonVersion:
        raise EnvironmentError(  # Enforce minimum Python version
            f"""
            Outdated Python!
            Installed Python Version: {sys.version}
            Minimum Python Version:   {".".join(map(str, minPythonVersion))}
            """
        )

    maxPythonVersion = (3, 12, 99)
    if sys.version_info > maxPythonVersion:
        raise EnvironmentError(  # Enforce maximum Python version
            f"""
            Python Too Advanced!
            Installed Python Version: {sys.version}
            Maximum Python Version:   {".".join(map(str, maxPythonVersion))}
            """
        )

    if os.name != "posix":
        raise EnvironmentError(  # Enforce POSIX os.name
            f"""
            Incompatible OS Family!
            Current OS Name:  {os.name}
            Required OS Name: posix
            """
        )

    if sys.platform != "linux":
        raise EnvironmentError(  # Enforce Linux sys.platform
            f"""
            Incompatible Platform!
            Current Platform:  {sys.platform}
            Required Platform: linux
            """
        )


def loopInit(log: eris.logger.logger) -> asyncio.AbstractEventLoop:
    """Initialize Asyncio Event Loop"""
    try:
        if os.environ["ERIS_LOOP_TYPE"] == "uvloop":
            uvloop.install()  # Initialize uvloop
            loop: asyncio.AbstractEventLoop = uvloop.new_event_loop()
        else:
            loop: asyncio.AbstractEventLoop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
    except Exception as e:
        log.critical("Unexpected exception occurred while initializing asyncio loop!", exc_info=e)
        raise e
    else:
        return loop


def botInit(loop: asyncio.AbstractEventLoop, handler: _handler) -> eris.bot.Bot:
    """Initialize eRis Bot"""
    bot = Bot(
        command_prefix=commands.when_mentioned_or(
            os.environ["ERIS_DISCORD_PREFIX"] if "ERIS_DISCORD_PREFIX" in os.environ else ""
        ),
        loop=loop,
        handler=handler,
    )

    bot.status = discord.Status.invisible

    return bot


def signalRegister(
    loop: asyncio.AbstractEventLoop, handler: _handler, bot: eris.bot.Bot, log: eris.logger.logger
) -> None:
    """Register signals for eRis Bot"""
    signals = {
        "shutdown": (signal.SIGTERM, signal.SIGINT),
        "stack-dump": (signal.SIGBUS, signal.SIGSEGV, signal.SIGFPE),
        "reload": (signal.SIGHUP, signal.SIGUSR1),
        "restart": (signal.SIGQUIT, signal.SIGABRT),
    }
    for sig in signals["shutdown"]:
        loop.add_signal_handler(
            sig,
            lambda sign=sig: loop.create_task(
                handler.shutdownHandler(
                    bot, cause=handler.shutdownCauses.caught_signal, signal=sign
                )
            ),
        )
    for sig in signals["stack-dump"]:
        loop.add_signal_handler(sig, lambda sign=sig: stackDumper(log, sig))
    for sig in signals["reload"]:
        loop.add_signal_handler(
            sig,
            lambda sign=sig: [
                log.warn(
                    f"Received {sig.name}, scheduling command tree sync for immediate execution..."
                ),
                bot.scheduler.schedule_task(coro=bot.tree.sync(), name="bot tree sync"),
            ],
        )

    for sig in signals["restart"]:
        loop.add_signal_handler(
            sig,
            lambda sign=sig: [
                log.warn(f"Restarting bot at request of {sig.name}..."),
                sys.exit(exit_codes.restart),
            ],
        )


async def print_version_output(log: eris.logger.logger) -> None:
    """Print running eRis version"""
    freeze_proc = await asyncio.create_subprocess_shell(
        cmd=f"{sys.executable} -m pip freeze", stdout=asyncio.subprocess.PIPE
    )

    stdout = (await freeze_proc.communicate())[0].decode().split("\n")

    version = ""
    for ver in stdout:
        if ver.startswith("eris=="):
            version = ver.replace("eris==", "").replace("\r", "")

    if version:
        log.info(f"Running eRis v{version}!")
    else:
        log.info("Running eRis outside of pip!")


def main() -> None:
    """
    Main initialization function
    """

    environmentVerify()

    log = logger("eRis.main")  # Initialize and keep a reference to our eRis.main logger
    discordLog = logger("discord")  # Initialize and keep a reference to discord.py's logger
    discordLog.setLevel(os.environ["ERIS_DPY_LOGGING_LEVEL"])

    handler = _handler()  # Initialize handler

    log.info("Hello World!")

    bot = None
    loop = loopInit(log=log)
    try:

        bot = botInit(loop=loop, handler=handler)

        signalRegister(loop=loop, handler=handler, bot=bot, log=log)

        loop.set_exception_handler(functools.partial(handler.taskExceptionHandler, bot))
        authTask = loop.create_task(eRisAuth(bot, log))
        authTask.add_done_callback(functools.partial(handler.botExceptionHandler, bot))
        loop.run_until_complete(authTask)
        log.info("Logged in!")
        bot.connTask = loop.create_task(bot.connect())
        initTask = loop.create_task(eRisInit(bot, log))
        initTask.add_done_callback(functools.partial(handler.botExceptionHandler, bot))
        bot.scheduler.schedule_task(
            coro=print_version_output(log=log), name="print version output"
        )
        loop.run_forever()
    except KeyboardInterrupt:
        log.warning(
            "Shouldn't you be running this as a service? Don't use keyboard interrupts to shutdown eRis!"
        )
        log.error("Treating received KeyboardInterrupt as SIGINT...")
        if bot:
            loop.run_until_complete(
                handler.shutdownHandler(
                    bot, cause=handler.shutdownCauses.caught_signal, signal=signal.SIGINT
                )
            )
    except SystemExit as syEx:
        if bot:
            loop.run_until_complete(
                handler.shutdownHandler(
                    bot,
                    cause=(
                        handler.shutdownCauses.user_requested_stop
                        if syEx.code == 0
                        else (
                            handler.shutdownCauses.user_requested_restart
                            if syEx.code == 39
                            else handler.shutdownCauses.unhandled_exception
                        )
                    ),
                    exception=syEx,
                )
            )
    except Exception as ex:
        if bot:
            loop.run_until_complete(
                handler.shutdownHandler(
                    bot, cause=handler.shutdownCauses.unhandled_exception, exception=ex
                )
            )
    finally:
        log.info("Cleaning up event loop...")
        loop.run_until_complete(loop.shutdown_asyncgens())
        asyncio.set_event_loop(None)
        loop.stop()
        loop.close()
        log.info("All done. Goodbye, World!")
    sys.exit(bot.exitCode if bot else 1)


if __name__ == "__main__":
    main()
