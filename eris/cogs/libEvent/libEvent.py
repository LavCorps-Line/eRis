#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# /cogs/libEvent
## a component of eRis
* handles all events for eRis
"""
# Standard Library Imports #
import io

# Third-Party Imports #
import discord
import discord.ext.commands as commands

# First-Party Imports #
import eris.bot
import eris.cog
from eris.cogs.libCore import libCore
from eris.logger import logger


class libEvent(eris.cog.Cog):
    """Event handler for eRis"""

    def __init__(self, **kwargs) -> None:
        self.bot: eris.bot.Bot = kwargs["bot"]
        self.log = logger("eRis.core.cogs.libEvent")
        self.core: libCore = kwargs["deps"]["libCore"]

    @eris.cog.Cog.listener(name="on_command_error")
    async def cmdErrHandler(
        self, ctx: commands.Context, error: commands.errors.CommandError
    ) -> None:
        """Command Error Handler, a component of eRis libEvent

        Parameters
        ----------
        ctx: `commands.Context`
        error: `commands.CommandError`
        """
        if isinstance(error, commands.errors.CommandNotFound):
            pass
        elif isinstance(error, commands.errors.UserInputError) and ctx.command:
            await ctx.send_help(ctx.command)
        elif isinstance(error, commands.errors.CheckFailure):
            await ctx.send(
                "Sorry, but you don't meet the requirements to use that command.", ephemeral=True
            )
        else:
            await ctx.send(
                content=(
                    f"Unhandled exception in command `{ctx.invoked_with}`!\n"
                    "Please consider sending the file attached below to a LavCorps-Line developer for debugging."
                ),
                file=discord.File(
                    io.BytesIO(
                        initial_bytes=self.log.formatException(
                            (type(error), error, error.__traceback__)
                        ).encode("utf-8")
                    ),
                    filename="traceback.txt",
                    description="Command Error Traceback",
                ),
                ephemeral=True,
            )

    @eris.cog.Cog.listener(name="on_connect")
    async def connectedHandler(self) -> None:
        """Connected Handler, a component of eRis libEvent"""
        self.log.info("Connected to Discord!")
        await self.bot.change_presence()

    @eris.cog.Cog.listener(name="on_disconnect")
    async def disconnectedHandler(self) -> None:
        """Disconnected Handler, a component of eRis libEvent"""
        self.log.warn("Disconnected from Discord!")

    @eris.cog.Cog.listener(name="on_resumed")
    async def resumedHandler(self) -> None:
        """Resumed Handler, a component of eRis libEvent"""
        self.log.info("Resumed connection to Discord!")
        await self.bot.change_presence()

    @eris.cog.Cog.listener(name="on_shard_connect")
    async def shardConnectedHandler(self, shard_id: int) -> None:
        """Shard Connected Handler, a component of eRis libEvent"""
        self.log.info(f"Shard {shard_id} connected to Discord!")
        await self.bot.change_presence()

    @eris.cog.Cog.listener(name="on_shard_disconnect")
    async def shardDisconnectedHandler(self, shard_id: int) -> None:
        """Shard Disconnected Handler, a component of eRis libEvent"""
        self.log.info(f"Shard {shard_id} disconnected from Discord!")

    @eris.cog.Cog.listener(name="on_shard_resumed")
    async def shardResumedHandler(self, shard_id: int) -> None:
        """Resumed Handler, a component of eRis libEvent"""
        self.log.info(f"Shard {shard_id} resumed connection to Discord!")
        await self.bot.change_presence()

    @eris.cog.Cog.listener(name="on_thread_create")
    async def threadCreationHandler(self, thread: discord.Thread) -> None:
        """Thread Creation Handler, a component of eRis libEvent"""
        try:
            await thread.join()
        except discord.Forbidden:
            pass
