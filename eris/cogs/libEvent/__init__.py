#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# /cogs/libEvent
## a component of eRis
* handles all events for eRis
"""

# First-Party Imports #
from eris.cog import CogImportData

# Local Imports #
from .libEvent import libEvent


def data(**kwargs) -> CogImportData:
    """Cog Import Data Shim"""
    return CogImportData(cog=libEvent(**kwargs))
