#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# /cogs/libConfig
## a component of eRis
* provides persistent configuration storage
"""

# First-Party Imports #
from eris.cog import CogImportData

# Local Imports #
from .libConfig import libConfig


def data(**kwargs) -> CogImportData:
    """Cog Import Data Shim"""
    return CogImportData(cog=libConfig(**kwargs))
