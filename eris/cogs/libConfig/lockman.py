#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# /cogs/libConfig/lockman.py
## a component of eRis
* handles lock managing for config access
"""

# Standard Library Imports #
import asyncio
import copy
import typing

# Third-Party Imports #
import discord.abc

# First-Party Imports #
import eris.bot
import eris.cog
import eris.cogs.libConfig.libConfig
import eris.exceptions


class lockManager(typing.AsyncContextManager[dict]):
    """A manager for snowflake ID locking."""

    def __init__(self, snf: discord.abc.Snowflake, bot: eris.bot.Bot, cog: eris.cog.Cog):
        if "libConfig" in bot.coreCogs:
            self.cfg: eris.cogs.libConfig.libConfig = bot.coreCogs["libConfig"]
        else:
            raise RuntimeError(
                "Tried to synchronously initialize lockManager, but libConfig is unloaded!"
            )
        self.id = snf.id
        self.bot = bot
        self.cog = cog.__data__.name
        if self.id not in self.cfg.locks:
            self.cfg.locks[self.id] = asyncio.Lock()
        self.lock = self.cfg.locks[self.id]
        self.data: dict = {}
        self.data_old: dict = {}
        self.log = self.bot._lockman_log

    async def __aenter__(self) -> dict:
        await self.lock.acquire()
        try:
            self.data = await self.cfg.read(self.id, self.cog)
        except eris.exceptions.DataNotFound:
            self.log.debug(f"{self.cog}:{self.id} uninitialized")
            pass
        self.data_old = copy.deepcopy(self.data)
        return self.data

    async def __aexit__(self, __exc_type, __exc_value, __traceback) -> bool:
        if __exc_value:
            self.log.exception(
                f"Unhandled exception occured while working with configuration for {self.cog}:{self.id}",
                exc_info=__exc_value,
            )
            self.lock.release()
            return False
        if self.data == self.data_old:
            self.lock.release()
            return True
        try:
            await self.cfg.write(self.id, self.cog, self.data)
        except Exception as sqlErr:  # TODO: Narrow except scope
            self.log.exception(
                "Unhandled exception occured while writing modified configuration for {self.cog}:{self.id}",
                exc_info=sqlErr,
            )
            self.lock.release()
            return False
        else:
            self.lock.release()
            return True
