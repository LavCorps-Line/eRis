#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# /cogs/libConfig
## a component of eRis
* provides persistent configuration storage
"""

# Standard Library Imports #
import asyncio
import os
import typing

# Third-Party Imports #
import discord
import discord.ext.commands as commands
import sqlalchemy.exc

# First-Party Imports #
import eris.bot
import eris.cog
import eris.cogs.libConfig.drivers.abc as abc
import eris.exceptions
from eris.cogs.libCore import libCore
from eris.logger import logger


class libConfig(eris.cog.Cog):
    """config handler for eRis"""

    def __init__(self, **kwargs) -> None:
        self.bot: eris.bot.Bot = kwargs["bot"]
        self.log = logger("eRis.core.cogs.libConfig")
        self._alchemylog = logger("sqlalchemy")
        if "ERIS_SQL_LOGGING_LEVEL" in os.environ:
            self._alchemylog.setLevel(os.environ["ERIS_SQL_LOGGING_LEVEL"])
        elif self.log.getEffectiveLevel() < 30:
            self.log.debug("increasing sqlalchemy log level")
            self._alchemylog.setLevel("WARNING")
        self.core: libCore = kwargs["deps"]["libCore"]
        self.locks: typing.Dict[int, asyncio.Lock] = {}
        match (os.environ["ERIS_CONFIG_TYPE"] if "ERIS_CONFIG_TYPE" in os.environ else None):
            case "PSQL":
                # First-Party Imports #
                from eris.cogs.libConfig.drivers import psql

                self.driver: abc.Driver = psql.Driver()
            case "SQLITE":
                # First-Party Imports #
                from eris.cogs.libConfig.drivers import sqlite

                self.driver: abc.Driver = sqlite.Driver()
            case _:
                self.log.critical(
                    """
                    No supported config type was provided to libConfig! The bot is unable to store ANY data!
                    To prevent unexpected behavior, the bot will now abort libConfig loading.
                    """
                )
                raise EnvironmentError("Invalid libConfig Config")
        self.read = self.driver.read
        self.write = self.driver.write
        self.delete = self.driver.delete
        self.janitorwaiting = False

    async def _cache_janitor(self) -> None:
        self.janitorwaiting = False
        self.log.debug("cache janitor running")
        self.driver.cache.cleanup()
        if self.bot.scheduler.operating and not self.janitorwaiting:
            self.log.debug("scheduling cache janitor for 1 hour from now...")
            self.bot.scheduler.schedule_task(self._cache_janitor(), 3600, "libconfig janitor")
            self.janitorwaiting = True

    async def __late_init__(self) -> None:
        """
        libConfig cog late init
        Initializes databases
        """
        try:
            await self.driver.__late_init__()
        except sqlalchemy.exc.OperationalError as opErr:
            self.log.exception(
                f"""
                libConfig FAILED late driver initialization! Panic...
                Driver URL was {str(self.driver.engine.url).replace(f":{os.environ['ERIS_PSQL_PASSWORD']}", "") if "ERIS_PSQL_PASSWORD" in os.environ else self.driver.engine.url}
                """,
                exc_info=opErr,
            )
            raise
        if self.bot.scheduler.operating and not self.janitorwaiting:
            self.log.debug("scheduling cache janitor for 1 hour from now...")
            self.bot.scheduler.schedule_task(self._cache_janitor(), 3600, "libconfig janitor")
            self.janitorwaiting = True

    async def __cleanup__(self) -> None:
        await self.driver.engine.dispose()

    @commands.hybrid_group(name="cache")
    @commands.is_owner()
    async def cachegroup(self, ctx: commands.Context) -> None:
        """eRis Cache Status Viewing"""
        pass

    @cachegroup.command()
    @commands.bot_has_permissions(embed_links=True)
    async def overview(self, ctx: commands.Context) -> None:
        """Overview cache entries"""
        async with ctx.typing(ephemeral=True):
            embed = discord.Embed(color=discord.Color.random(), title="Cache Overview")
            embed.add_field(name="Snowflakes cached", value=str(len(self.driver.cache.data)))
            await ctx.send(embed=embed, ephemeral=True)

    @cachegroup.command()
    async def touch(self, ctx: commands.Context, snf: int) -> None:
        """Perform a Unix-style touch on cached snowflakes.

        More specifically, this works by calling check() on every cog's cached data."""
        start = self.bot.loop.time()
        async with ctx.typing(ephemeral=True):
            if snf in self.driver.cache.data:
                ctr = 0
                for cname in self.driver.cache.data[snf]:
                    ctr += 1
                    self.driver.cache.check(snf, cname)
                if ctr == 0:
                    await ctx.send(f"`{snf}` not cached!")
                else:
                    await ctx.send(
                        f"Done!\n`Worked for {self.bot.loop.time() - start}`\n`Touched    {ctr}`",
                        ephemeral=True,
                    )
            else:
                await ctx.send(f"`{snf}` not cached!")

    @eris.cog.Cog.listener(name="on_guild_removed")
    async def _removed_guild_cleanup(self, guild: discord.Guild) -> None:
        """Clean up guild data from removed guild

        May be frustrating if bot is removed with the intention to be re-added,
        however this event reaction will help prevent useless configuration from building up,
        and can be utilized by server staff to clear broken or unwanted server configuration.

        Deletion is scheduled for 3 seconds in the future."""
        self.bot.scheduler.schedule_task(
            self.delete(guild.id), offset=3, name=f"cleanup-{guild.id}"
        )

    @eris.cog.Cog.listener(name="on_guild_channel_removed")
    async def _removed_channel_cleanup(self, channel: discord.abc.GuildChannel) -> None:
        """Clean up channel data from removed guild channel

        No qualms about this cleanup method,
        as channel removal has a much more concrete cause
        than guild removal.

        Deletion is scheduled for 3 seconds in the future."""
        self.bot.scheduler.schedule_task(
            self.delete(channel.id), offset=3, name=f"cleanup-{channel.id}"
        )

    @eris.cog.Cog.listener(name="on_message_delete")
    async def _removed_message_cleanup(self, message: discord.Message) -> None:
        """Clean up message data from deleted message

        No qualms about this cleanup method,
        as message deletion has a much more concrete cause
        than guild removal.

        Deletion is scheduled for 3 seconds in the future."""
        self.bot.scheduler.schedule_task(
            self.delete(message.id), offset=3, name=f"cleanup-{message.id}"
        )
