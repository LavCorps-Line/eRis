#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# /cogs/libConfig/drivers
## a component of eRis
* provides persistent configuration storage drivers
"""
# Standard Library Imports #
import asyncio
import copy
from collections import defaultdict as ddict

# Third-Party Imports #
import sqlalchemy
import sqlalchemy.ext.asyncio
from sqlalchemy import MetaData, Table, delete, insert, select, update

# First-Party Imports #
import eris.exceptions
from eris.logger import logger


class cacheEntry:
    """Class for storing data in a cache"""

    def __init__(self, data: dict, age: float):
        self.data = data
        self.age = age


class cache:
    """A manager for snowflake ID + cog name data caching."""

    def __init__(self):
        self.data: ddict[int, dict[str, cacheEntry]] = ddict(dict)
        self.log = logger("eRis.core.cogs.libConfig.cache")
        self.expectancy = 60 * 30

    def check(self, snf: str | int, cname: str) -> bool:
        """Checks a snowflake and a cog name combo for presence in cache.

        Args:
            snf: str | int
            cname: str

        Returns:
            bool

        """
        now = asyncio.get_running_loop().time()
        if snf in self.data:
            if cname in self.data[snf]:
                if not self.data[snf][cname].age + self.expectancy <= now:
                    self.log.debug(f"refreshing {snf}:{cname} age")
                    self.data[snf][cname].age = now
                    return True
                else:
                    self.log.debug(f"dropping {snf}:{cname}")
                    self.data[snf].pop(cname)
                    if len(self.data[snf]) == 0:
                        self.data.pop(snf)
                    return False
            else:
                return False
        else:
            return False

    def pull(self, snf: str | int, cname: str) -> dict:
        """Checks a snowflake and a cog name combo for presence in cache. If it's there, return the cached value.

        Args:
            snf: str | int
            cname: str

        Raises:
            IndexError

        Returns:
            dict

        """
        if self.check(snf, cname):
            return copy.deepcopy(self.data[snf][cname].data)
        else:
            raise IndexError("not cached!")

    def push(self, snf: str | int, cname: str, data: dict) -> None:
        """Pushes data into cache.

        Args:
            snf: str | int
            cname: str
            data: dict

        Returns:
            None

        """
        self.log.debug(f"updating {snf}:{cname}")
        self.data[snf][cname] = cacheEntry(
            data=copy.deepcopy(data), age=asyncio.get_running_loop().time()
        )

    def cleanup(self) -> int:
        culled = 0
        for i, _ in list(self.data.items()):
            for e in list(_.keys()):
                if not self.check(i, e):
                    culled += 1
        return culled


class Driver:
    """An ABC that details the common operations on persistent storage databases."""

    engine: sqlalchemy.ext.asyncio.AsyncEngine
    meta: MetaData
    table: Table
    log: logger
    cache: cache

    def __init__(self):
        self.cache = cache()

    async def __late_init__(self):
        conn: sqlalchemy.ext.asyncio.AsyncConnection
        self.log.info("Initializing generic storage driver...")
        async with self.engine.begin() as conn:
            await conn.run_sync(self.meta.create_all)
            await conn.commit()

    async def read(self, snf: str | int, cog: str) -> dict:
        """Reads from persistent storage.

        Args:
            snf: str | int
            cog: str

        Raises:
            eris.exceptions.DataNotFound

        Returns:
            dict

        """
        conn: sqlalchemy.ext.asyncio.AsyncConnection
        self.log.debug(f"Generic driver is reading ID {snf} on cog {cog}")
        if self.cache.check(snf, cog):
            self.log.debug("Pulling from cache.")
            return self.cache.pull(snf, cog)
        else:
            async with self.engine.connect() as conn:
                out = (
                    await conn.execute(
                        select(self.table).where(
                            self.table.c.ctx_snowflake == int(snf),
                            self.table.c.cog == cog,
                            self.table.c.json_data.isnot(None),
                        )
                    )
                ).fetchone()
            if out:
                self.log.debug("Pulling from database.")
                return out[2]
            else:
                raise eris.exceptions.DataNotFound()

    async def write(self, snf: str | int, cog: str, data: dict) -> None:
        """Writes to persistent storage.
        Transparently replaces existing data, if any.

        Args:
            snf: str | int
            cog: str
            data: dict

        Returns:
            None

        """
        conn: sqlalchemy.ext.asyncio.AsyncConnection
        self.log.debug(f"Generic driver is writing ID {snf} on cog {cog}")
        try:
            upstream = await self.read(snf, cog)
        except eris.exceptions.DataNotFound:
            self.log.debug("Data doesn't exist upstream")
            async with self.engine.connect() as conn:
                await conn.execute(
                    insert(self.table).values(
                        {"ctx_snowflake": int(snf), "cog": cog, "json_data": data}
                    )
                )
                await conn.commit()
        else:
            if data != upstream:
                self.log.debug(
                    "Data exists upstream, and is different from what we want to write."
                )
                async with self.engine.connect() as conn:
                    await conn.execute(
                        update(self.table)
                        .where(
                            self.table.c.ctx_snowflake == int(snf),
                            self.table.c.cog == cog,
                            self.table.c.json_data.isnot(None),
                        )
                        .values(json_data=data)
                    )
                    await conn.commit()
            else:
                self.log.debug("Data exists upstream, but is identical to what we want to write.")
        self.cache.push(snf, cog, data)

    async def delete(self, snf: str | int):
        """Deletes from persistent storage.

        Args:
            snf: str | int

        Returns:
            None

        """
        conn: sqlalchemy.ext.asyncio.AsyncConnection
        self.log.debug(f"Generic driver is deleting ID {snf}")
        async with self.engine.connect() as conn:
            await conn.execute(delete(self.table).where(self.table.c.ctx_snowflake == int(snf)))
            await conn.commit()
        if snf in self.cache.data:
            self.cache.data.pop(snf)
