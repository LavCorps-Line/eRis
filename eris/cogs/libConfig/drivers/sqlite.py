#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# /cogs/libConfig/drivers
## a component of eRis
* provides persistent configuration storage drivers
"""
# Standard Library Imports #
import os

# Third-Party Imports #
from sqlalchemy import JSON, BigInteger, Column, MetaData, Table, Text
from sqlalchemy.ext.asyncio import create_async_engine

# First-Party Imports #
import eris.cogs.libConfig.drivers.abc as driverbase
from eris.logger import logger


class Driver(driverbase.Driver):
    """SQLite Data Driver for eRis"""

    def __init__(self):
        super().__init__()
        self.log = logger("eRis.core.cogs.libConfig.drivers.sqlite")
        self.log.debug(
            f"""Driver is creating engine: sqlite+aiosqlite://{f'/{os.environ["ERIS_SQLITE_DBPATH"]}' if 'ERIS_SQLITE_DBPATH' in os.environ else ''}"""
        )
        self.engine = create_async_engine(
            f"""sqlite+aiosqlite://{f'/{os.environ["ERIS_SQLITE_DBPATH"]}' if 'ERIS_SQLITE_DBPATH' in os.environ else ''}"""
        )
        self.meta = MetaData()
        self.table = Table(
            os.environ["ERIS_SQLITE_TABLE"],
            self.meta,
            Column("ctx_snowflake", BigInteger()),
            Column("cog", Text()),
            Column("json_data", JSON()),
        )
