#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# /cogs/libConfig/drivers
## a component of eRis
* provides persistent configuration storage drivers
"""
# Standard Library Imports #
import os

# Third-Party Imports #
import sqlalchemy.ext.asyncio
from sqlalchemy import Column, MetaData, Table
from sqlalchemy.dialects.postgresql import BIGINT, JSONB, TEXT
from sqlalchemy.ext.asyncio import create_async_engine
from sqlalchemy.schema import CreateSchema

# First-Party Imports #
import eris.cogs.libConfig.drivers.abc as driverbase
from eris.logger import logger


class Driver(driverbase.Driver):
    """PostgreSQL Data Driver for eRis"""

    def __init__(self):
        super().__init__()
        self.log = logger("eRis.core.cogs.libConfig.drivers.psql")
        self.log.debug(
            f"Driver is creating engine (password omitted): postgresql+psycopg://{os.environ['ERIS_PSQL_USERNAME']}@{os.environ['ERIS_PSQL_HOST']}:{os.environ['ERIS_PSQL_PORT']}/{os.environ['ERIS_PSQL_DBNAME']}"
        )
        self.engine = create_async_engine(
            f"postgresql+psycopg://{os.environ['ERIS_PSQL_USERNAME']}:{os.environ['ERIS_PSQL_PASSWORD']}@{os.environ['ERIS_PSQL_HOST']}:{os.environ['ERIS_PSQL_PORT']}/{os.environ['ERIS_PSQL_DBNAME']}"
        )
        self.meta = MetaData(schema=os.environ["ERIS_PSQL_SCHEMA"])
        self.table = Table(
            os.environ["ERIS_PSQL_TABLE"],
            self.meta,
            Column("ctx_snowflake", BIGINT()),
            Column("cog", TEXT()),
            Column("json_data", JSONB()),
        )

    async def __late_init__(self):
        conn: sqlalchemy.ext.asyncio.AsyncConnection
        self.log.info("Initializing PostgreSQL storage driver...")
        async with self.engine.begin() as conn:
            await conn.execute(CreateSchema(os.environ["ERIS_PSQL_SCHEMA"], if_not_exists=True))
            await conn.run_sync(self.meta.create_all)
            await conn.commit()
