#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# /cogs/loader
## a component of eRis
* implements modular cog loading into eRis
* implements discord commands for loading cogs into eRis
"""

# First-Party Imports #
from eris.cog import CogImportData

# Local Imports #
from .loader import loader


def data(**kwargs) -> CogImportData:
    """Cog Import Data Shim"""
    return CogImportData(cog=loader(**kwargs))
