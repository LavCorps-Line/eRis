#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# /cogs/loader
## a component of eRis
* implements modular cog loading into eRis
* implements discord commands for loading cogs into eRis
"""

# Standard Library Imports #
import asyncio
import io
import pathlib
import sys

# Third-Party Imports #
import discord
import discord.ext.commands as commands

# First-Party Imports #
import eris.bot
import eris.cog
import eris.exceptions as exceptions
from eris.cogs.libConfig.lockman import lockManager
from eris.cogs.libCore import libCore
from eris.logger import logger

loadCmdCtxInfo = {
    exceptions.CogNotFound: "Consider double-checking the cog name.",
    exceptions.DependencyNotLoaded: "Are all dependencies loaded?",
    discord.ClientException: "Are you sure the cog hasn't been loaded already?",
}

unloadCmdCtxInfo = {
    exceptions.CogNotFound: "Has the cog already been unloaded?",
    exceptions.CoreUnloadAttempt: "Are you attempting to unload a core cog?",
}


class loader(eris.cog.Cog):
    """loader from eRis
    Handles cog loading and unloading"""

    def __init__(self, **kwargs) -> None:
        self.bot: eris.bot.Bot = kwargs["bot"]
        self.log = logger("eRis.core.cogs.loader")
        self.core: libCore = kwargs["deps"]["libCore"]

    async def __late_init__(self) -> None:
        """
        Loader cog late init
        Reads all saved cogs, and attempts to load them
        """
        self.log.info("Loading saved cogs...")
        async with lockManager(snf=self.bot.user, bot=self.bot, cog=self) as cfg:
            if not cfg or "cogs" not in cfg:
                self.log.info("No cogs saved for startup loading.")
            else:
                cogs: list[dict] = cfg["cogs"]
                cog: dict[str, str]
                for cog in cogs:
                    self.bot.scheduler.schedule_task(
                        coro=self.__load_cog(cog["cogName"], cog["cogPath"]), name=f"load cog {cog['cogName']}"
                    )  # Load them
                await asyncio.sleep(len(cogs) * 0.2)  # Give them a little extra time to load
        self.log.info(
            msg=(
                "Here's what the current command tree looks like:\n"
                f"{str([command.name for command in await self.bot.tree.fetch_commands()])}"
            )
        )

    async def __cleanup__(self) -> None:
        """
        Loader cog cleanup
        Saves all currently loaded auxiliary cogs to reload upon next startup
        Unloads all currently loaded auxiliary cogs
        """
        self.log.info("Cleaning up all non-core cogs...")
        async with lockManager(snf=self.bot.user, bot=self.bot, cog=self) as cfg:
            if "cogs" not in cfg:
                cfg["cogs"] = []
            cogs: list[dict] = cfg["cogs"]
            cogs.clear()
            for aCogName, aCog in self.bot.auxCogs.items():
                cogs.append({"cogName": aCogName, "cogPath": aCog.__data__.data_path})

        cgs = [cog for cog in self.bot.auxCogs]  # Init aux cogs list
        cgs.sort(key=lambda c: self.bot.auxCogs[c].__data__.priority)  # Sort our cogs list
        iterations = 0  # Init our iterations count
        maxIterations = len(self.bot.auxCogs) * 2  # Init our maxIterations count
        while len(cgs) != 0:
            iterations += 1  # Increase our iterations count
            if iterations >= maxIterations:
                raise RecursionError(  # Throw an error if we've gone around this loop too many times
                    f"""
                    Iteration count {iterations} exceeds allowance!
                    We're failing to unload a core cog's dependents!
                    """
                )
            cogDepData = self.bot.dependencies_dict(core=False)
            cog = (
                cgs.pop()
            )  # Remove cog from cogs list, returning highest priority cog first.
            if cogDepData[cog]["dependents"]:  # If cog still has active dependents...
                cgs.append(cog)  # Put it back in the cog list
                continue
            await self.__unload_cog(cog)
        self.log.info("All cleaned up!")

    async def __load_cog(
        self, cogName: str, cogPath: str | pathlib.Path, _retry: bool = True
    ) -> None:
        """
        Load a cog

        Parameters
        ----------
        cogName: `str`
            Name of the cog to add to Bot.auxCogs, as well as the path to the cog from data.toml if unspecified
        cogPath: `pathlib.Path`
            Path to dir containing data.toml
        _retry: `bool`
            Internal use only
            Set to True to allow 1 retry
            Set to False to allow 0 retries
        """
        if isinstance(cogPath, pathlib.Path):
            cogPath = cogPath.resolve()  # Resolve path
        elif isinstance(cogPath, str):
            cogPath = pathlib.Path(
                cogPath
            ).resolve()  # Convert path to pathlib.Path, then resolve it
        _cogPath = cogPath
        cogData: eris.cog.CogData | None = None
        for cogDef in eris.cog.CogData.__import__(
            (
                (cogPath / "data.toml") if not str(cogPath).endswith("/data.toml") else cogPath
            ).resolve()
        ):
            if cogDef.name == cogName:
                cogData = cogDef
                break
        if not cogData:
            raise exceptions.CogNotFound(
                f"Tried to load {cogName} from {cogPath}, but {cogName} wasn't found!"
            )
        self.log.debug(f"Loading {cogData.name} from {cogData.init_path}...")
        try:
            await self.bot.__load_cog__(cogData)
        except exceptions.DependencyNotLoaded:
            if _retry:
                self.log.warn(
                    f"Tried to load cog {cogName}, but dependencies aren't loaded. Retrying in 10 seconds..."
                )
                self.bot.scheduler.schedule_task(
                    coro=self.__load_cog(cogName=cogName, cogPath=cogPath, _retry=False),
                    offset=10,
                    name=f"load cog {cogName}",
                )
                return
            else:
                self.log.exception("Tried to load a cog but we don't have its dependency loaded!")
                raise
        except discord.ClientException:
            raise
        self.bot.scheduler.schedule_task(
            coro=self.bot.auxCogs[cogName].__late_init__(), name=f"{cogName} __late_init__"
        )  # Schedule cog's __late_init__()

    async def __unload_cog(self, cogName: str) -> None:
        """
        Unload a cog

        Parameters
        ----------
        cogName: `str`
            Name of the cog as stored in Bot.auxCogs
        """
        if (
            cogName in self.bot.coreCogs
        ):  # Verify the cog we're told to unload isn't loaded as a coreCog
            self.log.exception(f"Tried to unload cog {cogName}, but it's a core cog!")
            raise exceptions.CoreUnloadAttempt(
                f"Tried to unload cog {cogName}, but it's a core cog!"
            )
        elif (
            cogName not in self.bot.auxCogs
        ):  # Verify the cog we're told to unload is loaded as an auxCog
            self.log.exception(f"Tried to unload cog {cogName}, but it wasn't loaded!")
            raise exceptions.CogNotFound(f"Tried to unload cog {cogName}, but it wasn't loaded!")
        else:
            await self.bot.auxCogs[cogName].__cleanup__()  # Let the cog clean itself up
            await self.bot.auxCogs[cogName].__data__.unload(
                cog=self.bot.auxCogs[cogName]
            )  # Call the cog's unloader
            sys.modules.pop(cogName)  # Remove the cog from sys.modules

    @commands.hybrid_command()
    @commands.is_owner()
    async def load(self, ctx: commands.Context, cog_name: str, cog_path: str, sync: bool = True):
        """Load a cog with name cog_name from the given cog_path.

        cog_name: `str`
           * Machine-readable name of cog defined in cog's `data.toml`
        cog_path: `str`
           * Path to cog's `data.toml` or directory containing `data.toml`
        sync: `bool`
           * Whether or not to sync the command tree after loading.
        """
        async with ctx.typing(ephemeral=True):
            self.log.info(
                f"Loading cog {cog_name} from {cog_path} at request of {ctx.author.display_name}..."
            )
            try:
                if not cog_path.endswith("/data.toml"):
                    cog_path += "/data.toml"
                cog_path = pathlib.Path(cog_path).resolve()
                if not cog_path.exists():
                    raise FileNotFoundError("Given cog_path doesn't exist!")
            except (OSError, RuntimeError, FileNotFoundError) as err:
                await ctx.send(
                    "Failed to resolve path to cog.\nTraceback is attached below.",
                    file=discord.File(
                        io.BytesIO(
                            initial_bytes=self.log.formatException(
                                (type(err), err, err.__traceback__)
                            ).encode("utf-8")
                        ),
                        filename="traceback.txt",
                        description="Command Error Traceback",
                    ),
                    ephemeral=True,
                )
                self.log.exception("Failed to resolve path to cog.", exc_info=err)
            else:
                try:
                    await self.__load_cog(cog_name, cog_path, _retry=False)
                except (
                    exceptions.DependencyNotLoaded,
                    exceptions.CogNotFound,
                    discord.ClientException,
                ) as err:
                    await ctx.send(
                        f"Failed to load cog. {loadCmdCtxInfo[type(err)]}\nTraceback is attached below.",
                        file=discord.File(
                            io.BytesIO(
                                initial_bytes=self.log.formatException(
                                    (type(err), err, err.__traceback__)
                                ).encode("utf-8")
                            ),
                            filename="traceback.txt",
                            description="Command Error Traceback",
                        ),
                        ephemeral=True,
                    )
                    self.log.exception(
                        "Failed to load cog. {loadCmdCtxInfo[type(err)]}",
                        exc_info=err,
                    )
                    try:
                        await self.__unload_cog(cog_name)
                    except exceptions.CogNotFound:
                        pass
                else:
                    if sync:
                        try:
                            await self.bot.tree.sync()
                        except (
                            discord.HTTPException,
                            discord.Forbidden,
                            discord.app_commands.MissingApplicationID,
                            discord.app_commands.TranslationError,
                        ) as err:
                            await ctx.send(
                                "Failed to sync command tree to Discord.\nTraceback is attached below.",
                                file=discord.File(
                                    io.BytesIO(
                                        initial_bytes=self.log.formatException(
                                            (type(err), err, err.__traceback__)
                                        ).encode("utf-8")
                                    ),
                                    filename="traceback.txt",
                                    description="Command Error Traceback",
                                ),
                                ephemeral=True,
                            )
                            self.log.exception(
                                "Failed to sync command tree to Discord.", exc_info=err
                            )
                            await self.__unload_cog(cog_name)
                        else:
                            await ctx.send("Done!", ephemeral=True)
                    else:
                        await ctx.send("Done!", ephemeral=True)

    @commands.hybrid_command()
    @commands.is_owner()
    async def unload(self, ctx: commands.Context, cog_name: str, sync: bool = True):
        """Unload a cog with name cog_name.

        cog_name: `str`
           * Machine-readable name of cog defined in cog's `data.toml`
        sync: `bool`
           * Whether or not to sync the command tree after unloading.
        """
        async with ctx.typing(ephemeral=True):
            self.log.info(f"Unloading cog {cog_name} at request of {ctx.author.display_name}...")
            try:
                await self.__unload_cog(cog_name)
            except (exceptions.CoreUnloadAttempt, exceptions.CogNotFound) as err:
                await ctx.send(
                    f"Failed to unload cog. {unloadCmdCtxInfo[type(err)]}\nTraceback is attached below.",
                    file=discord.File(
                        io.BytesIO(
                            initial_bytes=self.log.formatException(
                                (type(err), err, err.__traceback__)
                            ).encode("utf-8")
                        ),
                        filename="traceback.txt",
                        description="Command Error Traceback",
                    ),
                    ephemeral=True,
                )
            else:
                if sync:
                    try:
                        await self.bot.tree.sync()
                    except (
                        discord.HTTPException,
                        discord.Forbidden,
                        discord.app_commands.MissingApplicationID,
                        discord.app_commands.TranslationError,
                    ) as err:
                        await ctx.send(
                            "Failed to sync command tree to Discord.\nTraceback is attached below.",
                            file=discord.File(
                                io.BytesIO(
                                    initial_bytes=self.log.formatException(
                                        (type(err), err, err.__traceback__)
                                    ).encode("utf-8")
                                ),
                                filename="traceback.txt",
                                description="Command Error Traceback",
                            ),
                            ephemeral=True,
                        )
                    else:
                        await ctx.send("Done!", ephemeral=True)
                else:
                    await ctx.send("Done!", ephemeral=True)

    @commands.hybrid_command()
    @commands.is_owner()
    async def cogs(self, ctx: commands.Context):
        """List all cogs currently loaded."""
        await ctx.send(
            self.core.naturalize.list.listContents(
                [self.bot.allCogs[cog].__data__.human_name for cog in self.bot.allCogs]
            ),
            ephemeral=True,
        )
