#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# /cogs/libCore/eRisHelpCommand.py
## a component of eRis
* provides the main help command class for eRis
"""

# Standard Library Imports #
import asyncio
import datetime
import itertools

# Third-Party Imports #
import discord
import discord.ext.commands as cmds

# First-Party Imports #
import eris.cog


class eRisHelpCommand(cmds.HelpCommand):
    """eRisHelpCommand from libCore"""

    def __init__(self, **kwargs):
        self.desc: str = kwargs.pop("desc", "Placeholder Description")
        self.img: str = kwargs.pop("img", "https://raster.shields.io/badge/-Placeholder-grey.png")
        self.name: str = kwargs.pop("name", "eRis")
        self.sort_commands: bool = kwargs.pop("sort_commands", True)
        self.dm_help: bool = kwargs.pop("dm_help", True)
        self.dm_help_threshold: int = kwargs.pop("dm_help_threshold", 0)
        self.no_category: str = kwargs.pop("no_category", "No Category")
        self.embedColor: tuple[int] = kwargs.pop(
            "embed_color",
            (discord.Color.purple().r, discord.Color.dark_purple().g, discord.Color.purple().b),
        )
        self.embeds: asyncio.Queue[discord.Embed] | None = None
        super().__init__(**kwargs)

    @property
    def endingNote(self) -> str:
        """Returns the ending note for the help command"""
        return " ".join(
            (
                "Type {cleanPrefix}{commandName} command for more info on a command.",
                "You can also type {cleanPrefix}{commandName} category for more info on a category.",
            )
        ).format(
            cleanPrefix=self.context.clean_prefix if hasattr(self.context, "clean_prefix") else "",
            commandName=self.invoked_with,
        )

    def embedFactory(
        self, title: str | None = None, description: str | None = None
    ) -> discord.Embed:
        """
        Create an embed and return it

        Parameters
        ----------
        title: `str`
            Title for the returned embed
        description: `str`
            Description for the returned embed

        Returns
        -------
        discord.Embed
        """
        embed = discord.Embed(
            color=discord.Color.from_rgb(
                self.embedColor[0], self.embedColor[1], self.embedColor[2]
            ),
            title=title if title else self.name,
            description=description if description else self.desc,
            timestamp=datetime.datetime.now(
                tz=datetime.datetime.now(datetime.timezone.utc).astimezone().tzinfo
            ),
        )
        embed.set_author(name=self.name, icon_url=self.img)
        embed.set_footer(text=self.endingNote, icon_url=self.img)
        return embed

    async def addIndentedCommands(
        self,
        commands: list[cmds.Command],
        /,
        *,
        heading: str | None = None,
        body: str | None = None,
    ) -> None:
        """Indents a list of commands after the given heading

        Parameters
        ----------
        commands: `list[discord.ext.commands.Command]`
            list of commands to format and add to embed list
        heading: `str`
            heading of embed
        body: `str`
            body of embed
        """
        if not commands:
            return

        embed = self.embedFactory(title=heading, description=body)
        for command in commands:
            embed.add_field(name=command.qualified_name, value=command.short_doc)
        await self.embeds.put(embed)

    async def sendEmbeds(self) -> None:
        """Send pages from the paginator to the destination"""
        destination = self.get_destination()
        while self.embeds.qsize() > 10:
            embedList = []
            for x in range(10):
                embedList.append(await self.embeds.get())
                self.embeds.task_done()
            await destination.send(embeds=embedList)
        while 10 >= self.embeds.qsize() >= 1:
            embedList = []
            for x in range(self.embeds.qsize()):
                embedList.append(await self.embeds.get())
                self.embeds.task_done()
            await destination.send(embeds=embedList)

    async def addCommandFormatting(self, command: cmds.Command, /) -> None:
        """Format individual commands

        Parameters
        ----------
        command: `discord.ext.commands.Command`
        """
        embed = self.embedFactory(title=command.qualified_name)

        if command.description:
            embed.add_field(name="Description", value=command.description)
        if command.help:
            embed.add_field(name="Help", value=command.help)
        if self.get_command_signature(command):
            embed.add_field(name="Signature", value=self.get_command_signature(command))

        await self.embeds.put(embed)

    def get_destination(self) -> discord.abc.Messageable:
        """
        Returns destination for help message

        Returns
        -------
        discord.abc.Messageable
        """
        if self.dm_help is True:
            return self.context.author
        elif self.dm_help is None and self.embeds.qsize() > self.dm_help_threshold:
            return self.context.author
        else:
            return self.context.channel

    async def prepare_help_command(self, ctx: cmds.Context, command: str | None = None, /) -> None:
        """
        initialize help command runner

        Parameters
        ----------
        ctx: `discord.ext.commands.Context`
            context for help command
        command: `str`
        """
        self.embeds = asyncio.Queue()  # set up our embeds queue
        await super().prepare_help_command(ctx, command)  # propagate our prepare_help_command

    # async def send_error_message(self, error: str, /) -> None:
    # pass

    async def send_bot_help(self, mapping, /) -> None:
        """
        main command to send bot-wide help

        Parameters
        ----------
        mapping
        """
        ctx = self.context
        bot = ctx.bot

        noCategory = f"\u200b{self.no_category}"

        filtered = await self.filter_commands(
            bot.commands,
            sort=True,
            key=lambda command, no_category=noCategory: (
                command.cog.__data__.name if command.cog else no_category
            ),
        )
        toIterate = itertools.groupby(
            filtered,
            key=lambda command, no_category=noCategory: (
                command.cog.__data__.name if command.cog else no_category
            ),
        )

        for category, commands in toIterate:
            commands = (
                sorted(commands, key=lambda command: command.cog.__data__.human_name)
                if self.sort_commands
                else list(commands)
            )

            if bot.get_cog(category) and bot.get_cog(category).description:
                await self.addIndentedCommands(
                    commands, heading=category, body=bot.cogs[category].description
                )
            else:
                await self.addIndentedCommands(commands, heading=category)
        await self.sendEmbeds()

    async def send_command_help(self, command: cmds.Command, /) -> None:
        """
        main command to send command-specific help

        Parameters
        ----------
        command: `discord.ext.commands.Command`
        """
        await self.addCommandFormatting(command)
        await self.sendEmbeds()

    async def send_group_help(self, group: cmds.Group, /) -> None:
        """
        main command to send group-specific help

        Parameters
        ----------
        group: `discord.ext.commands.Group`
        """
        await self.addCommandFormatting(group)

        filtered = await self.filter_commands(group.commands, sort=self.sort_commands)
        await self.addIndentedCommands(
            filtered, heading=group.qualified_name, body=group.description
        )
        await self.sendEmbeds()

    async def send_cog_help(self, cog: eris.cog.Cog, /) -> None:
        """
        main command to send cog-specific help

        Parameters
        ----------
        cog: `eris.cog.Cog`
        """
        filtered = await self.filter_commands(cog.get_commands(), sort=self.sort_commands)
        if cog.description:
            await self.addIndentedCommands(
                filtered, heading=cog.__data__.human_name, body=cog.description
            )
        else:
            await self.addIndentedCommands(filtered, heading=cog.__data__.human_name)
        await self.sendEmbeds()
