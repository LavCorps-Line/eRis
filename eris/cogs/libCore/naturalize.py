#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# /cogs/libCore/naturalize.py
## a component of eRis
* provides tie-ins to humanize
* provides tie-ins to list humanizing
"""

# Standard Library Imports #
import datetime
import sys

# Third-Party Imports #
import humanize


class naturalize:
    """naturalize component of libCore from eRis"""

    def __init__(self) -> None:
        self.number = (
            humanize.number
        )  # provide reference to humanize.number as libCore.naturalize.number
        self.time = humanize.time  # provide reference to humanize.time as libCore.naturalize.time
        self.filesize = (
            humanize.filesize
        )  # provide reference to humanize.filesize as libCore.naturalize.filesize
        self.list = natList(
            self.number, self.time
        )  # provide reference to natList as libCore.naturalize.list


class natList:
    """naturalize: list component of libCore from eRis"""

    def __init__(self, number, time) -> None:
        self.number = number  # provide reference to humanize.number internally
        self.time = time  # provide reference to humanize.time internally

    def listContents(self, rawList: list, truncate: bool = False) -> str:
        """Humanize a list

        Parameters
        ----------
        rawList: `list`
            List of arbitrary objects
        truncate: `bool`
            If set to true, do not include items which are not truthy in the output.

        Returns
        -------
        `str`
            Human-readable string of list contents"""
        try:
            if not rawList:
                return ""
            strList = []  # initialize strList, list of strings
            for i in range(
                len(rawList)
            ):  # give index for this iteration, so we can nicely use commas in output
                if truncate and not rawList[i]:
                    continue
                if isinstance(rawList[i], str):
                    item = rawList[i]  # perform no operations on items that are already strings
                elif isinstance(rawList[i], int) or isinstance(rawList[i], float):
                    item = self.number.intcomma(rawList[i])  # make ints and floats look pretty
                elif isinstance(rawList[i], datetime.timedelta):
                    item = self.time.naturaldelta(rawList[i])  # make timedeltas look pretty
                else:
                    item = str(rawList[i])  # call rawList[i].__str__() and hope for the best

                if len(rawList) == 1:
                    strList.append(item)
                elif i == (len(rawList) - 1):
                    strList.append(f"and {item}")  # end of iteration; no extra comma needed
                else:
                    strList.append(f"{item}, ")  # not at the end of iteration yet, put in a comma
            return "".join(
                strList
            )  # output includes an oxford comma; it just takes more code to not include one
        except KeyError:
            print(str(rawList), file=sys.stderr)
            raise
