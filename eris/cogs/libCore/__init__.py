#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# /cogs/libCore
## a component of eRis
* Provides utilty library for other cogs
* Exposes naturalize.py
* Exposes chronologize.py
* provides ping command
* provides restart command
* provides shutdown command
* provides invite command
* provides help command
"""

# First-Party Imports #
from eris.cog import CogImportData

# Local Imports #
from .libCore import libCore


def data(**kwargs) -> CogImportData:
    """Cog Import Data Shim"""
    return CogImportData(cog=libCore(**kwargs))
