#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# /cogs/libCore/chronologize.py
## a component of libCore
* provides time management utilities
"""

# Standard Library Imports #
import datetime

# Third-Party Imports #
import dateparser


class chronologize:
    """chronologize component of libCore from eRis"""

    def __init__(self) -> None:
        self.dateparser = dateparser

    @property
    def localTz(self) -> datetime.tzinfo:
        """Return local timezone as defined by a naive datetime timestamp according to system time"""
        return datetime.datetime.now(datetime.timezone.utc).astimezone().tzinfo

    def now(self, tz: datetime.tzinfo = None) -> datetime.datetime:
        """Return timezone-aware current datetime"""
        if not tz:
            tz = self.localTz
        return datetime.datetime.now(tz=tz)

    def isoNow(self, tz: datetime.tzinfo = None) -> str:
        """Return timezone-aware current datetime in ISO-8601 format"""
        if not tz:
            tz = self.localTz
        return self.now(tz).isoformat()

    def posixNow(self, tz: datetime.tzinfo = None) -> float:
        """Return timezone-naive current datetime in POSIX timestamp format"""
        if not tz:
            tz = self.localTz
        return self.now(tz).timestamp()
