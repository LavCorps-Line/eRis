#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# /cogs/libCore
## a component of eRis
* Provides utilty library for other cogs
* Exposes naturalize.py
* Exposes chronologize.py
* provides ping command
* provides restart command
* provides shutdown command
* provides invite command
* provides help command
"""

# Standard Library Imports #
import inspect
import re
import string
import sys
from typing import Any, Awaitable, Callable

# Third-Party Imports #
import discord
import discord.ext.commands as commands

# First-Party Imports #
import eris.bot
import eris.cog
from eris.logger import logger
from eris.shared import exit_codes

# Local Imports #
from .chronologize import chronologize
from .eRisHelpCommand import eRisHelpCommand
from .naturalize import naturalize


class list_differences:
    """List Differences from eRis

    Aids in listing differences in lists"""

    def __init__(self, before: list, now: list, after: list):
        self._before: list = before
        self._now: list = now
        self._after: list = after

    @property
    def add(self):
        """Return list of items ADDED to the list."""
        return self._after

    @property
    def added(self):
        """Return list of items ADDED to the list."""
        return self._after

    @property
    def after(self):
        """Return list of items ADDED to the list."""
        return self._after

    @property
    def now(self):
        """Return list of items UNCHANGED in the list."""
        return self._now

    @property
    def unchanged(self):
        """Return list of items UNCHANGED in the list."""
        return self._now

    @property
    def remove(self):
        """Return list of items REMOVED from the list."""
        return self._before

    @property
    def removed(self):
        """Return list of items REMOVED from the list."""
        return self._before

    @property
    def before(self):
        """Return list of items REMOVED from the list."""
        return self._before


class libCore(eris.cog.Cog):
    """libCore from eRis

    Provides common utilities to cogs"""

    def __init__(self, **kwargs) -> None:
        self.bot: eris.bot.Bot = kwargs["bot"]
        self.log = logger("eRis.core.cogs.libCore")
        self.naturalize = naturalize()  # provide reference to naturalize as libCore.naturalize
        self.chronologize = (
            chronologize()
        )  # provide reference to chronologize as libCore.chronologize
        self.randomColor = discord.Color.random
        self.bot.help_command = eRisHelpCommand(
            desc=self.bot.description, img=self.bot.user.avatar.url, name=self.bot.user.name
        )
        self.bot.help_command.cog = self

    @commands.hybrid_command()
    async def ping(self, ctx: commands.Context) -> None:
        """Ping the bot! Provides response time, Discord WebSocket latency, and scheduler latency."""
        ping = await ctx.send("Pong!", ephemeral=True)
        self.bot.scheduler.schedule_task(
            coro=ping.edit(
                content=(
                    "Pong!\n"
                    "`response time: "
                    f"{str((ping.created_at - ctx.message.created_at).seconds) + 's' if (ping.created_at - ctx.message.created_at).seconds > 0 else ''}"
                    f"{(ping.created_at - ctx.message.created_at).microseconds / 1000}ms`\n"
                    "`discord latency: "
                    f"{int(self.bot.latency * 1000 if -(2 ** 31) - 1.0 < self.bot.latency < 2 ** 31 - 1.0 else 0)}ms`\n"
                    "`scheduler latency: "
                    f"{(self.chronologize.now() - ping.created_at).microseconds / 1000}ms`\n"
                )
            ),
            name="ping",
        )

    @commands.hybrid_command()
    async def invite(self, ctx: commands.Context) -> None:
        """Send an invite link for the bot"""
        await ctx.send(
            f"https://discord.com/oauth2/authorize?client_id={self.bot.user.id}&scope=applications.commands%20bot",
            ephemeral=True,
        )

    @commands.hybrid_command()
    @commands.is_owner()
    async def sync(self, ctx: commands.Context) -> None:
        """Manually sync bot command tree."""
        async with ctx.typing(ephemeral=True):
            self.log.info(f"Syncing command tree at request of {ctx.author.display_name}...")
            await self.bot.tree.sync()
            await ctx.send("Done!", ephemeral=True)

    @commands.hybrid_command()
    @commands.is_owner()
    async def stop(self, ctx: commands.Context) -> None:
        """Stop the bot with a code intended to signal init to stop eRis."""
        self.log.info(f"Stopping bot at request of {ctx.author.display_name}...")
        await ctx.send(
            f"Understood! Stopping with code `{exit_codes.shutdown}`...", ephemeral=True
        )
        sys.exit(exit_codes.shutdown)

    @commands.hybrid_command()
    @commands.is_owner()
    async def restart(self, ctx: commands.Context) -> None:
        """Stop the bot with a code intended to signal init to restart eRis."""
        self.log.info(f"Restarting bot at request of {ctx.author.display_name}...")
        await ctx.send(f"Understood! Stopping with code `{exit_codes.restart}`...", ephemeral=True)
        sys.exit(exit_codes.restart)

    @staticmethod
    def shortenText(
        text: str,
        width: int = 80,
        whitespace: list | tuple = tuple(" "),
        breakspace: list | tuple | None = None,
        joinspace: str = " ",
    ) -> str:
        """Smartly shortens input text to fit width, breaking on whitespaces and breakspaces"""
        if breakspace:
            _breakspace = breakspace
        else:
            _breakspace = tuple(
                [delimiter for delimiter in string.whitespace if delimiter not in whitespace]
            )
        _text = re.split("|".join(whitespace), text)
        _outText = ""
        for word in _text:
            if word in _breakspace:
                return _outText
            if len(word) + len(_outText) + 3 >= width:
                _outText += "..."
                return _outText
            elif not _outText:
                _outText += word
            else:
                _outText += joinspace + word
        return _outText

    @staticmethod  # Alternatively, we can do all of this in one line:
    def splitText(
        inText: str,
    ) -> list:  # splitText = lambda s: list(map(lambda e: e[:-1], s.split("(")))
        """Split text.

        Parameters
        ----------
        inText: `str`
            i.e. "A (B)"

        Returns
        -------
        `list`
            i.e. ["A", "B"]
        """
        outText = inText.split(
            "("
        )  # Split the input text on the beginning parenthesis; ["A ", "B)"]
        outText[0] = outText[0][:-1]  # Split the trailing space off the first string; ["A", "B)"]
        outText[1] = outText[1][
            :-1
        ]  # Split the trailing end parenthesis off the second string; ["A", "B"]
        return outText

    @staticmethod
    def listDiff(before: list, after: list) -> list_differences:
        """Get the difference between two lists

        Parameters
        ----------
        before: `list`
            i.e. ["a", "b", "c", 1, 2, 3]
        after: `list`
            i.e. ["x", "y", "z", 1, 2, 3]

        Returns
        -------
        `dict`
            i.e.
            {
                "added": ["x", "y", "z"]
                "removed": ["a", "b", "c"]
                "unchanged": [1, 2, 3]
            }
        """
        added = []  # Initialize the "added" list
        removed = []  # Initialize the "removed" list
        unchanged = []  # Initialize the "unchanged" list
        for item in before:  # For items in the "before" list...
            if item not in after:  # If the item is not in the "after" list...
                removed.append(item)  # It was removed, and will be added to the "removed" list
        for item in after:  # For items in the "after" list...
            if item not in before:  # If the item is not in the "before" list...
                added.append(item)  # It was added, and will be added to the "added" list
        for item in before:  # For items in the "before" list...
            if item in after:  # If the item is in the "after" list...
                unchanged.append(
                    item
                )  # It was unchanged, and will be added to the "unchanged" list
        return list_differences(before=removed, now=unchanged, after=added)

    @staticmethod
    def pagifyString(
        inStr: str, pageLength: int = 1000, whitespace: list | tuple = tuple(string.whitespace)
    ) -> str:
        """Pagify long strings, splitting at (by default) whitespaces every (by default) 1000 characters.
        Be mindful of unicode.

        Parameters
        ----------
        inStr: `str`
            Block of text to split
        pageLength: `int`
            How long should a "page" of text be, default 1000
        whitespace: `list`

        Yields
        ------
        `list`"""
        inStrList = re.split("|".join(whitespace), inStr)  # Construct our list of strings.
        inStrList.reverse()  # Invert our list of strings, so we can pop off strings in order later on.
        outStrList = []  # Init output string list.
        while len(inStrList) != 0:  # While inStrList is not empty...
            strItem = inStrList.pop()  # Pop a string out of inStrList
            if strItem in whitespace or not strItem:
                continue
            if (
                len(outStrList) == 0 or len(outStrList[-1]) + len(strItem) > pageLength
            ):  # If outStrList is empty OR if the latest string in outStrList exceeds page length...
                outStrList.append(strItem)  # Append popped string to list
            else:
                outStrList[-1] = " ".join(
                    [outStrList[-1], strItem]
                )  # Replace the latest string in outStrList with joined latest string and popped string.
        for item in outStrList:  # For strings in outStrList...
            yield item  # Yield as generator object.

    @staticmethod
    async def execute(x: Callable[..., Any] | Awaitable[Any]) -> Any:
        """Handle execution for any callable or awaitable object, and return the results."""
        if inspect.isawaitable(x):
            return await x
        else:
            return x
