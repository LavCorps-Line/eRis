#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# /exceptions.py
## a component of eRis
* defines exceptions used in the bot
"""


class CoreUnloadAttempt(RuntimeError):
    """Thrown when a core cog is attempted to be unloaded.

    Differentiated from RuntimeError to distinguish from other RuntimeErrors"""


class CogNotFound(ImportError):
    """Thrown when cog isn't found

    Differentiated from ImportError to distinguish from errors that occur while actually importing modules
    """


class DependencyNotLoaded(ImportError):
    """Thrown when cog's dependencies aren't loaded

    Differentiated from ImportError to distinguish from errors that occur while actually importing modules
    """


class DataNotFound(ValueError):
    """Thrown when libConfig is asked to read data, but no data matches the criteria given.

    Differentiated from SQLite3 or Psycopg errors
    as the read operation completed successfully, but returned no data."""


class ConfigOperationalError(Exception):
    """Thrown when libConfig encounters an OperationalError.

    Catchall for psycopg.OperationalError and sqlite3.OperationalError"""
