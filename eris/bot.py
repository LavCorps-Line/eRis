#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# /bot.py
## a component of eRis
* core logic for the bot
"""

# Standard Library Imports #
import asyncio
import importlib.util
import sys

# Third-Party Imports #
import aiodns
import aiohttp
import discord.ext.commands

# First-Party Imports #
import eris.cog
import eris.exceptions as exceptions
import eris.scheduler
from eris.logger import logger


class Bot(discord.ext.commands.bot.AutoShardedBot):
    """
    eRis Bot Core, a component of eRis
    """

    def __init__(self, *args, **kwargs):
        self.exitCode = 1
        self.log = logger("eRis.core")
        self._lockman_log = logger("eRis.core.cogs.libConfig.lockman")
        self.loop: asyncio.AbstractEventLoop = kwargs["loop"]
        self.httpClient: aiohttp.ClientSession = aiohttp.ClientSession(loop=self.loop)
        self.dnsClient: aiodns.DNSResolver = aiodns.DNSResolver(loop=self.loop)
        self.connTask: asyncio.Future | None = None
        self.handler = kwargs["handler"]
        self.scheduler = eris.scheduler.Scheduler()
        self.scheduler.bot = self
        self.coreCogs: dict[str, eris.cog.Cog] = {}
        self.auxCogs: dict[str, eris.cog.Cog] = {}
        super().__init__(*args, help_command=None, intents=discord.Intents.all(), **kwargs)

    @property
    def allCogs(self) -> dict[str, eris.cog.Cog]:
        """Return union of self.auxCogs and self.coreCogs"""
        return {**self.auxCogs, **self.coreCogs}

    def dependencies_dict(
        self, core: bool | None = None
    ) -> dict[str, dict[str, list[str]], dict[str, list[str]]]:
        """Return a dict of dependencies and dependents for all loaded cogs

        Parameters
        ----------
        core: `bool` | `None`
            `core` is a tribool, allowing a state that is either True, False, or None.
        """
        depDict: dict[str, dict[str, list[str]], dict[str, list[str]]] = {}
        for cog in (
            self.allCogs if core is None else self.auxCogs if core is False else self.coreCogs
        ):
            cogData: eris.cog.CogData = (
                self.allCogs if core is None else self.auxCogs if core is False else self.coreCogs
            )[cog].__data__
            depDict[cogData.name] = {}
            depDict[cogData.name]["dependencies"] = cogData.depends
            depDict[cogData.name]["dependents"] = []
        for cog in depDict:
            cogData: eris.cog.CogData = (
                self.allCogs if core is None else self.auxCogs if core is False else self.coreCogs
            )[cog].__data__
            for secondaryCog in depDict:
                secondaryCogData: eris.cog.CogData = (
                    self.allCogs
                    if core is None
                    else self.auxCogs if core is False else self.coreCogs
                )[secondaryCog].__data__
                if cogData.name in secondaryCogData.depends:
                    depDict[cogData.name]["dependents"].append(secondaryCogData.name)
        return depDict

    async def __load_cog__(self, cog: eris.cog.CogData, core: bool = False, **kwargs) -> None:
        for dependency in cog.depends:
            if dependency not in self.allCogs:
                raise exceptions.DependencyNotLoaded(
                    f"We're missing dependency for cog {cog.name}!"
                )  # Raise exception if dependencies aren't loaded
        if cog.name in self.allCogs:
            raise discord.ClientException(f"Cog {cog.name} already loaded!")
        try:
            spec = importlib.util.spec_from_file_location(
                cog.name, str(cog.init_path)
            )  # Create spec
        except Exception as err:
            self.log.critical(
                f"Failed to create spec for {cog.name} from {cog.init_path}!",
                exc_info=err,
            )
            raise
        try:
            module = importlib.util.module_from_spec(spec)  # Create module
        except Exception as err:
            self.log.critical(
                f"Failed to create module for {cog.name} from {cog.init_path}!",
                exc_info=err,
            )
            raise
        sys.modules[cog.name] = module  # Add module to sys.modules
        try:
            spec.loader.exec_module(module)  # Execute module
        except Exception as err:
            self.log.critical(
                f"Failed to create spec for {cog.name} from {cog.init_path}!",
                exc_info=err,
            )
            raise

        dependencies = {}  # Initialize empty dependencies dict
        for dependency in cog.depends:
            dependencies[dependency] = self.allCogs[dependency]  # Populate dependencies
        loading_args = {
            "bot": self,
            "deps": dependencies,
            "path": kwargs["path"] if "path" in kwargs else cog.init_path,
            "core": core,
        }
        try:
            cogImportData: eris.cog.CogImportData = module.data(**loading_args)
            loading_args["cog"] = cogImportData.cog
            loading_args["load"] = (
                cogImportData.custom_load if cogImportData.custom_load else self.register_cog
            )
            loading_args["unload"] = (
                cogImportData.custom_unload if cogImportData.custom_unload else self.unregister_cog
            )
            await loading_args["load"](**loading_args)
        except Exception as ex:
            self.log.exception("Unhandled exception in cog loading!", exc_info=ex)
            sys.modules.pop(cog.name)
            raise
        cog.load = loading_args["load"]
        cog.unload = loading_args["unload"]
        if not cog.description and module.__doc__:
            cog.description = module.__doc__
        (self.coreCogs if core else self.auxCogs)[cog.name].__data__ = cog

    async def register_cog(
        self, cog: eris.cog.Cog | None = None, core: bool = False, **kwargs
    ) -> None:
        """
        Register a cog into eRis

        Parameters
        ----------
        cog: `eris.cog.Cog`
            Cog object to load
        core: `bool`
            Toggle whether the core is a cog core or not
        """

        if not isinstance(
            cog, eris.cog.Cog
        ):  # Verify the cog we were given is an instance of eris.cog.Cog
            raise TypeError(
                f"eRis Cogs must derive from eris.cog.Cog, was given {str(type(cog) if cog != type else cog)}"
            )
        if cog.qualified_name in self.allCogs:
            raise discord.ClientException(
                f"The cog name {cog.__data__.human_name} ({cog.qualified_name}) already exists!"
            )
        await self.add_cog(cog)  # Add cog to internal discord.py stuff
        (self.coreCogs if core else self.auxCogs)[cog.qualified_name] = cog

    async def unregister_cog(self, cog: eris.cog.Cog, core: bool = False, **kwargs) -> None:
        """
        Unregister a cog from eRis

        Parameters
        ----------
        cog: `eris.cog.Cog`
            Name of the cog to unregister
        core: `bool`
            Toggle whether cog is a core cog or not
        """
        (self.coreCogs if core else self.auxCogs).pop(cog.qualified_name)
        await self.remove_cog(cog.qualified_name)
