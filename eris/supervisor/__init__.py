#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# /supervisor/__init__.py
## a component of eRis Supervisor
"""

# Local Imports #
from .__main__ import main
