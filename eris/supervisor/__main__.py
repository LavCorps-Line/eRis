#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# /supervisor/__main__.py
## a component of eRis Supervisor
* handles restart, crashes, and stopping of eRis
"""

# Standard Library Imports #
import asyncio
import grp
import os
import pathlib
import pwd
import signal
import sys

# Third-Party Imports #
import toml

if "ERIS_LOOP_TYPE" in os.environ and os.environ["ERIS_LOOP_TYPE"] == "uvloop":
    try:
        # Third-Party Imports #
        import uvloop
    except ImportError as impErr:
        print("Tried to import uvloop, but ImportError was raised! Are you sure it's installed?")
        raise impErr
elif "ERIS_LOOP_TYPE" not in os.environ:
    os.environ["ERIS_LOOP_TYPE"] = "asyncio"

# First-Party Imports #
from eris.shared import exit_codes


class Supervisor:
    """Supervisor class for an eRis Process"""

    def __init__(self, loop: asyncio.AbstractEventLoop):
        self.loop: asyncio.AbstractEventLoop = loop
        for sig in signal.valid_signals():
            if sig == 9 or sig == 17 or sig == 19:
                continue
            self.loop.add_signal_handler(sig, lambda s=sig: self.forward_signal(s))
        self.tasks: set = set()
        self.eRis: asyncio.subprocess.Process | None = None
        self.supervisor: asyncio.Task | None = None

    @staticmethod
    def supervisor_callback(task: asyncio.Future) -> None:
        """Supervisor function callback for eRis Supervisor"""
        asyncio.gather(task, return_exceptions=True)

    def forward_signal(self, sig: signal.Signals) -> None:
        """Forwards signal to child eRis process

        Parameters
        ----------
        sig: `signal`
            Signal to forward to eRis
        """
        if not self.eRis:
            return
        self.eRis.send_signal(signal=sig)

    async def spawn_eris(self):
        """Spawns eRis"""
        if self.eRis:
            return
        if "CI_JOB_NAME_SLUG" in os.environ and "test" in os.environ["CI_JOB_NAME_SLUG"]:
            self.eRis = await asyncio.create_subprocess_exec(
                "eRis",
                stdin=asyncio.subprocess.DEVNULL,
                stderr=asyncio.subprocess.PIPE,
                stdout=asyncio.subprocess.PIPE,
            )
        else:
            self.eRis = await asyncio.create_subprocess_exec(
                "eRis", stdin=sys.stdin, stderr=sys.stderr, stdout=sys.stdout
            )

    async def supervise_eris(self) -> None:
        """eRis Supervisor Function"""
        if not self.eRis:
            return
        if "CI_JOB_NAME_SLUG" in os.environ and "test" in os.environ["CI_JOB_NAME_SLUG"]:
            await self.eRis.communicate()
            retCode: int = self.eRis.returncode
        else:
            retCode: int = await self.eRis.wait()
        if retCode == exit_codes.restart or (
            retCode == exit_codes.error and os.environ["ERIS_RESTART_ON_CRASH"] == "True"
        ):
            self.eRis = None
            await self.spawn_eris()
            self.supervisor = self.loop.create_task(self.supervise_eris())
            self.supervisor.add_done_callback(self.supervisor_callback)
            return
        else:
            self.eRis = None
            self.supervisor = None
            if "CI_JOB_NAME_SLUG" in os.environ and "test" in os.environ["CI_JOB_NAME_SLUG"]:
                pass
            else:
                sys.exit(retCode)


class eRisEnvironment:
    """eRis Environment Parser, a component of eRis Supervisor"""

    def __init__(self, **kwargs):
        self.env = {
            # environment vars used in __main__.py
            "ERIS_LOOP_TYPE": (
                kwargs["ERIS_LOOP_TYPE"] if "ERIS_LOOP_TYPE" in kwargs else "asyncio"
            ),
            "ERIS_DISCORD_TOKEN": kwargs["ERIS_DISCORD_TOKEN"],
            "ERIS_DISCORD_PREFIX": (
                kwargs["ERIS_DISCORD_PREFIX"] if "ERIS_DISCORD_PREFIX" in kwargs else ""
            ),
            # environment vars used in logger.py
            "ERIS_LOGGING_LEVEL": (
                kwargs["ERIS_LOGGING_LEVEL"] if "ERIS_LOGGING_LEVEL" in kwargs else "WARNING"
            ),
            "ERIS_DPY_LOGGING_LEVEL": (
                kwargs["ERIS_DPY_LOGGING_LEVEL"]
                if "ERIS_DPY_LOGGING_LEVEL" in kwargs
                else "WARNING"
            ),
            "ERIS_SQL_LOGGING_LEVEL": (
                kwargs["ERIS_SQL_LOGGING_LEVEL"]
                if "ERIS_DPY_LOGGING_LEVEL" in kwargs
                else "WARNING"
            ),
            "ERIS_LOGGING_DIR": (
                kwargs["ERIS_LOGGING_DIR"] if "ERIS_LOGGING_DIR" in kwargs else "null"
            ),
            # environment vars used in cogs/libConfig/libConfig.py
            "ERIS_CONFIG_TYPE": kwargs["ERIS_CONFIG_TYPE"],
            "ERIS_PSQL_DBNAME": (
                kwargs["ERIS_PSQL_DBNAME"] if kwargs["ERIS_CONFIG_TYPE"] == "PSQL" else ""
            ),
            "ERIS_PSQL_USERNAME": (
                kwargs["ERIS_PSQL_USERNAME"]
                if "ERIS_PSQL_USERNAME" in kwargs
                else "eris" if kwargs["ERIS_CONFIG_TYPE"] == "PSQL" else ""
            ),
            "ERIS_PSQL_PASSWORD": (
                kwargs["ERIS_PSQL_PASSWORD"] if kwargs["ERIS_CONFIG_TYPE"] == "PSQL" else ""
            ),
            "ERIS_PSQL_HOST": (
                kwargs["ERIS_PSQL_HOST"]
                if "ERIS_PSQL_HOST" in kwargs
                else "localhost" if kwargs["ERIS_CONFIG_TYPE"] == "PSQL" else ""
            ),
            "ERIS_PSQL_PORT": (
                kwargs["ERIS_PSQL_PORT"]
                if "ERIS_PSQL_PORT" in kwargs
                else "5432" if kwargs["ERIS_CONFIG_TYPE"] == "PSQL" else ""
            ),
            "ERIS_PSQL_TABLE": (
                kwargs["ERIS_PSQL_TABLE"]
                if "ERIS_PSQL_TABLE" in kwargs
                else "eris" if kwargs["ERIS_CONFIG_TYPE"] == "PSQL" else ""
            ),
            "ERIS_PSQL_SCHEMA": (
                kwargs["ERIS_PSQL_SCHEMA"]
                if "ERIS_PSQL_SCHEMA" in kwargs
                else "eris" if kwargs["ERIS_CONFIG_TYPE"] == "PSQL" else ""
            ),
            "ERIS_SQLITE_DBPATH": (
                kwargs["ERIS_SQLITE_DBPATH"] if kwargs["ERIS_CONFIG_TYPE"] == "SQLITE" else ""
            ),
            "ERIS_SQLITE_TABLE": (
                kwargs["ERIS_SQLITE_TABLE"]
                if "ERIS_SQLITE_TABLE" in kwargs
                else "eris" if kwargs["ERIS_CONFIG_TYPE"] == "SQLITE" else ""
            ),
            # environment vars used in supervisor/__main__.py
            "ERIS_RESTART_ON_CRASH": (
                kwargs["ERIS_RESTART_ON_CRASH"] if "ERIS_RESTART_ON_CRASH" in kwargs else "False"
            ),
            "ERIS_USER": kwargs["ERIS_USER"] if "ERIS_USER" in kwargs else "root",
            "ERIS_GROUP": (
                kwargs["ERIS_GROUP"]
                if "ERIS_GROUP" in kwargs
                else kwargs["ERIS_USER"] if "ERIS_USER" in kwargs else "root"
            ),
        }


def environmentVerify() -> None:
    """Verify system environment"""
    minPythonVersion = (3, 10, 0)
    if sys.version_info < minPythonVersion:
        raise EnvironmentError(  # Enforce minimum Python version
            f"""
            Outdated Python!
            Installed Python Version: {sys.version}
            Minimum Python Version:   {".".join(map(str, minPythonVersion))}
            """
        )

    maxPythonVersion = (3, 12, 99)
    if sys.version_info > maxPythonVersion:
        raise EnvironmentError(  # Enforce maximum Python version
            f"""
            Python Too Advanced!
            Installed Python Version: {sys.version}
            Maximum Python Version:   {".".join(map(str, maxPythonVersion))}
            """
        )

    if os.name != "posix":
        raise EnvironmentError(  # Enforce POSIX os.name
            f"""
            Incompatible OS Family!
            Current OS Name:  {os.name}
            Required OS Name: posix
            """
        )

    if sys.platform != "linux":
        raise EnvironmentError(  # Enforce Linux sys.platform
            f"""
            Incompatible Platform!
            Current Platform:  {sys.platform}
            Required Platform: linux
            """
        )


def loopInit() -> asyncio.AbstractEventLoop:
    """Initialize Asyncio Event Loop"""
    if os.environ["ERIS_LOOP_TYPE"] == "uvloop":
        uvloop.install()  # Initialize uvloop
        loop: asyncio.AbstractEventLoop = uvloop.new_event_loop()
    else:
        loop: asyncio.AbstractEventLoop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    return loop


def loadEnvironment() -> eRisEnvironment:
    """
    Returns:
        eRisEnvironment
    """
    for path in ["/etc/eris"]:
        if pathlib.Path(path).resolve().exists():
            with open(path) as tmpFile:
                etcEris = toml.load(tmpFile)
                for var in etcEris:
                    if var in os.environ:
                        continue
                    os.environ[var] = str(etcEris[var])
    environment = eRisEnvironment(**dict(os.environ))
    for var in environment.env:
        os.environ[var] = environment.env[var]
    return environment


def setPerms(environment: eRisEnvironment):
    """Appropriately set running permissions."""
    if os.getuid() == 0:
        os.setgroups([])
        os.setgid(grp.getgrnam(environment.env["ERIS_GROUP"]).gr_gid)
        os.setuid(pwd.getpwnam(environment.env["ERIS_USER"]).pw_uid)
        os.umask(0o077)


def main() -> None:
    """Main initialization function"""
    environmentVerify()

    loop = loopInit()

    environment = loadEnvironment()

    setPerms(environment=environment)

    supervisor = Supervisor(loop=loop)
    loop.run_until_complete(loop.create_task(supervisor.spawn_eris()))
    first_supervisor = loop.create_task(supervisor.supervise_eris())
    first_supervisor.add_done_callback(supervisor.supervisor_callback)
    try:
        loop.run_forever()
    except SystemExit:
        for task in asyncio.all_tasks(loop):
            asyncio.gather(task, return_exceptions=True)
        raise


if __name__ == "__main__":
    main()
